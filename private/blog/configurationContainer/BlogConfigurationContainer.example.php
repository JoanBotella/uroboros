<?php
declare(strict_types=1);

namespace blog\configurationContainer;

use uroboros\configuration\Configuration;
use uroboros\configuration\ConfigurationItf;
use uroboros\configurationContainer\ConfigurationContainerAbs;
use uroboros\configurationContainer\ConfigurationContainerItf;
use uroboros\localization\LanguageCodeConstant;

final class BlogConfigurationContainer extends ConfigurationContainerAbs implements ConfigurationContainerItf
{

	protected function build():ConfigurationItf
	{
		return new Configuration(
			'http://localhost/uroboros/repo/',
			LanguageCodeConstant::ENGLISH,
			[
				LanguageCodeConstant::ENGLISH,
				LanguageCodeConstant::CASTILLIAN,
				LanguageCodeConstant::CATALAN,
			]
		);
	}

}