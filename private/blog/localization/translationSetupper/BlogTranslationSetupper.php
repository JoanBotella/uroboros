<?php
declare(strict_types=1);

namespace blog\localization\translationSetupper;

use uroboros\localization\languageCodeGuesser\LanguageCodeGuesserItf;
use uroboros\request\RequestItf;
use uroboros\localization\translationSetupper\TranslationSetupperItf;

use blog\localization\translationContainer\BlogTranslationContainerItf;

final class BlogTranslationSetupper implements TranslationSetupperItf
{

	private LanguageCodeGuesserItf $languageCodeGuesser;
	private BlogTranslationContainerItf $translationContainer;

	public function __construct(
		LanguageCodeGuesserItf $languageCodeGuesser,
		BlogTranslationContainerItf $translationContainer
	)
	{
		$this->languageCodeGuesser = $languageCodeGuesser;
		$this->translationContainer = $translationContainer;
	}

	public function setup(RequestItf $request):void
	{
		$languageCode = $this->languageCodeGuesser->guess($request);
		$this->translationContainer->setCurrentLanguageCode($languageCode);
	}

}