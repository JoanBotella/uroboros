<?php
declare(strict_types=1);

namespace blog\localization\translation;

use uroboros\localization\LanguageCodeConstant;

use blog\localization\translation\BlogTranslationItf;
use blog\zone\front\localization\translation\BlogFrontTranslationCaTrait;

final class BlogTranslationCa implements BlogTranslationItf
{
	use
		BlogFrontTranslationCaTrait
	;

	public function getLanguageCode():string
	{
		return LanguageCodeConstant::CATALAN;
	}

	public function blog_siteTitle():string { return 'El Blog de Joan Botella'; }

	public function blog_goTo(string $pageTitle):string { return 'Anar a &quot;'.$pageTitle.'&quot;'; }

}