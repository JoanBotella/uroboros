<?php
declare(strict_types=1);

namespace blog\localization\translation;

use uroboros\localization\LanguageCodeConstant;

use blog\localization\translation\BlogTranslationItf;
use blog\zone\front\localization\translation\BlogFrontTranslationEsTrait;

final class BlogTranslationEs implements BlogTranslationItf
{
	use
		BlogFrontTranslationEsTrait
	;

	public function getLanguageCode():string
	{
		return LanguageCodeConstant::CASTILLIAN;
	}

	public function blog_siteTitle():string { return 'El Blog de Joan Botella'; }

	public function blog_goTo(string $pageTitle):string { return 'Ir a &quot;'.$pageTitle.'&quot;'; }

}