<?php
declare(strict_types=1);

namespace blog\localization\translation;

use uroboros\localization\translation\TranslationItf;

use blog\zone\front\localization\translation\BlogFrontTranslationItf;

interface BlogTranslationItf
extends
	TranslationItf,
	BlogFrontTranslationItf
{

	public function blog_siteTitle():string;

	public function blog_goTo(string $pageTitle):string;

}