<?php
declare(strict_types=1);

namespace blog\localization\translation;

use uroboros\localization\LanguageCodeConstant;

use blog\localization\translation\BlogTranslationItf;
use blog\zone\front\localization\translation\BlogFrontTranslationEnTrait;

final class BlogTranslationEn implements BlogTranslationItf
{
	use
		BlogFrontTranslationEnTrait
	;

	public function getLanguageCode():string
	{
		return LanguageCodeConstant::ENGLISH;
	}

	public function blog_siteTitle():string { return 'Joan Botella\'s Blog'; }

	public function blog_goTo(string $pageTitle):string { return 'Go to &quot;'.$pageTitle.'&quot;'; }

}