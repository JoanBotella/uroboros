<?php
declare(strict_types=1);

namespace blog\localization\translationFactory;

use uroboros\localization\LanguageCodeConstant;

use blog\localization\translation\BlogTranslationCa;
use blog\localization\translation\BlogTranslationEn;
use blog\localization\translation\BlogTranslationEs;
use blog\localization\translationFactory\BlogTranslationFactoryItf;
use blog\localization\translation\BlogTranslationItf;

final class BlogTranslationFactory implements BlogTranslationFactoryItf
{

	public function make(string $languageCode):BlogTranslationItf
	{
		switch ($languageCode)
		{
			case LanguageCodeConstant::ENGLISH:
				return new BlogTranslationEn();

			case LanguageCodeConstant::CASTILLIAN:
				return new BlogTranslationEs();

			case LanguageCodeConstant::CATALAN:
				return new BlogTranslationCa();
		}

		// !!! any other must throw an exception
	}

}