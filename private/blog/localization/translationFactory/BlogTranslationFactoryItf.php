<?php
declare(strict_types=1);

namespace blog\localization\translationFactory;

use blog\localization\translation\BlogTranslationItf;

interface BlogTranslationFactoryItf
{

	public function make(string $languageCode):BlogTranslationItf;

}