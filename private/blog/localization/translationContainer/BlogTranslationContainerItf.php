<?php
declare(strict_types=1);

namespace blog\localization\translationContainer;

use blog\localization\translation\BlogTranslationItf;

interface BlogTranslationContainerItf
{

	public function setCurrentLanguageCode(string $languageCode):void;

	public function get():BlogTranslationItf;

	public function getByLanguageCode(string $languageCode):BlogTranslationItf;

}
