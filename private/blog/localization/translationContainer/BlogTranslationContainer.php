<?php
declare(strict_types=1);

namespace blog\localization\translationContainer;

use blog\localization\translation\BlogTranslationItf;
use blog\localization\translationFactory\BlogTranslationFactoryItf;

final class BlogTranslationContainer implements BlogTranslationContainerItf
{

	/**
	 * @var array<string, BlogTranslationItf> $translations
	 */
	private array $translations;
	private string $currentLanguageCode;

	private BlogTranslationFactoryItf $translationFactory;

	public function __construct(
		BlogTranslationFactoryItf $translationFactory
	)
	{
		$this->translationFactory = $translationFactory;
		$this->translations = [];
	}

	public function setCurrentLanguageCode(string $currentLanguageCode):void
	{
		$this->currentLanguageCode = $currentLanguageCode;
	}

	public function get():BlogTranslationItf
	{
		return $this->getByLanguageCode($this->currentLanguageCode);
	}

	public function getByLanguageCode(string $languageCode):BlogTranslationItf
	{
		if (!array_key_exists($languageCode, $this->translations))
		{
			$this->translations[$languageCode] = $this->translationFactory->make($languageCode);
		}
		return $this->translations[$languageCode];
	}

}