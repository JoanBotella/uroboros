<?php
declare(strict_types=1);

namespace blog\matcher;

use blog\localization\translation\BlogTranslationItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;

abstract class BlogMatcherAbs
{

	private BlogTranslationContainerItf $translationContainer;

	public function __construct(
		BlogTranslationContainerItf $translationContainer
	)
	{
		$this->translationContainer = $translationContainer;
	}

	protected function getTranslation():BlogTranslationItf
	{
		return $this->translationContainer->get();
	}

}