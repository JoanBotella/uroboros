<?php
declare(strict_types=1);

namespace blog\routeGeneratorsGenerator;

use Generator;

use uroboros\zone\page\route\RouteItf;
use uroboros\routeGeneratorsGenerator\RouteGeneratorsGeneratorItf;

use blog\dependencyInjector\BlogDependencyInjectorItf;

final class BlogRouteGeneratorsGenerator implements RouteGeneratorsGeneratorItf
{

	private BlogDependencyInjectorItf $dependencyInjector;

	public function __construct(
		BlogDependencyInjectorItf $dependencyInjector
	)
	{
		$this->dependencyInjector = $dependencyInjector;
	}

	/**
	 * @return Generator<Generator<RouteItf>>
	 */
	public function generate():Generator
	{
		yield $this->dependencyInjector->getBlogFrontRouteGenerator();
	}

}