<?php
declare(strict_types=1);

namespace blog\urlBuilder;

use blog\localization\translation\BlogTranslationItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use uroboros\configurationContainer\ConfigurationContainerItf;

abstract class BlogUrlBuilderAbs
{

	private ConfigurationContainerItf $configurationContainer;
	private BlogTranslationContainerItf $translationContainer;

	public function __construct(
		ConfigurationContainerItf $configurationContainer,
		BlogTranslationContainerItf $translationContainer
	)
	{
		$this->configurationContainer = $configurationContainer;
		$this->translationContainer = $translationContainer;
	}

	protected function getBaseUrl():string
	{
		return $this->configurationContainer->get()->getBaseUrl();
	}

	protected function getTranslation(string $languageCode):BlogTranslationItf
	{
		return $this->translationContainer->getByLanguageCode($languageCode);
	}

}