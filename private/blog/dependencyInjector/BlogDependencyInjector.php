<?php
declare(strict_types=1);

namespace blog\dependencyInjector;

use uroboros\dependencyInjector\DependencyInjectorTrait;
use uroboros\configurationContainer\ConfigurationContainerItf;
use uroboros\routeGeneratorsGenerator\RouteGeneratorsGeneratorItf;
use uroboros\localization\languageCodeGuesser\LanguageCodeGuesser;
use uroboros\localization\languageCodeGuesser\LanguageCodeGuesserItf;
use uroboros\localization\translationSetupper\TranslationSetupperItf;
use uroboros\router\Router;
use uroboros\router\RouterItf;

use blog\dependencyInjector\BlogDependencyInjectorItf;
use blog\zone\front\dependencyInjector\BlogFrontDependencyInjectorTrait;
use blog\configurationContainer\BlogConfigurationContainer;
use blog\localization\translationContainer\BlogTranslationContainer;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use blog\localization\translationFactory\BlogTranslationFactory;
use blog\localization\translationFactory\BlogTranslationFactoryItf;
use blog\localization\translationSetupper\BlogTranslationSetupper;
use blog\routeGeneratorsGenerator\BlogRouteGeneratorsGenerator;

final class BlogDependencyInjector implements BlogDependencyInjectorItf
{
	use
		DependencyInjectorTrait,
		BlogFrontDependencyInjectorTrait
	;

	private function getTranslationSetupper():TranslationSetupperItf
	{
		return new BlogTranslationSetupper(
			$this->getLanguageCodeGuesser(),
			$this->getTranslationContainer()
		);
	}

		private function getLanguageCodeGuesser():LanguageCodeGuesserItf
		{
			return new LanguageCodeGuesser(
				$this->getConfigurationContainer()
			);
		}

		private BlogTranslationContainerItf $translationContainer;

		private function getTranslationContainer():BlogTranslationContainerItf
		{
			if (!isset($this->translationContainer))
			{
				$this->translationContainer = new BlogTranslationContainer(
					$this->getTranslationFactory()
				);
			}
			return $this->translationContainer;
		}

			private function getTranslationFactory():BlogTranslationFactoryItf
			{
				return new BlogTranslationFactory();
			}

	private function getRouter():RouterItf
	{
		return new Router(
			$this->getRouteGeneratorsGenerator(),
			$this->getBlogFrontNotFoundControllerProxy()
		);
	}

		private function getRouteGeneratorsGenerator():RouteGeneratorsGeneratorItf
		{
			return new BlogRouteGeneratorsGenerator(
				$this
			);
		}

	private function getConfigurationContainer():ConfigurationContainerItf
	{
		return new BlogConfigurationContainer();
	}

}