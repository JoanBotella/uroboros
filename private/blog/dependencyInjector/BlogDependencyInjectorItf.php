<?php
declare(strict_types=1);

namespace blog\dependencyInjector;

use uroboros\dependencyInjector\DependencyInjectorItf;
use blog\zone\front\dependencyInjector\BlogFrontDependencyInjectorItf;

interface
	BlogDependencyInjectorItf
extends
	DependencyInjectorItf,
	BlogFrontDependencyInjectorItf
{
}