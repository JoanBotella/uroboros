<?php
declare(strict_types=1);

namespace blog\zone\front\routeGenerator;

use Generator;

use uroboros\zone\page\route\RouteItf;
use uroboros\zone\routeGenerator\RouteGeneratorItf;

use blog\zone\front\page\aboutMe\route\BlogFrontAboutMeRoute;
use blog\zone\front\page\aboutBlog\route\BlogFrontAboutBlogRoute;
use blog\zone\front\page\home\route\BlogFrontHomeRoute;
use blog\zone\front\dependencyInjector\BlogFrontDependencyInjectorItf;
use blog\zone\front\page\contact\route\BlogFrontContactRoute;
use blog\zone\front\page\notFound\route\BlogFrontNotFoundRoute;

final class BlogFrontRouteGenerator implements RouteGeneratorItf
{

	private BlogFrontDependencyInjectorItf $dependencyInjector;

	public function __construct(
		BlogFrontDependencyInjectorItf $dependencyInjector
	)
	{
		$this->dependencyInjector = $dependencyInjector;
	}

	/**
	 * @return Generator<RouteItf>
	 */
	public function generate():Generator
	{
		yield new BlogFrontHomeRoute($this->dependencyInjector);
		yield new BlogFrontAboutMeRoute($this->dependencyInjector);
		yield new BlogFrontAboutBlogRoute($this->dependencyInjector);
		yield new BlogFrontNotFoundRoute($this->dependencyInjector);
		yield new BlogFrontContactRoute($this->dependencyInjector);
	}

}