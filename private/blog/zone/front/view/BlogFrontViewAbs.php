<?php
declare(strict_types=1);

namespace blog\zone\front\view;

use uroboros\view\html\HtmlViewAbs;
use uroboros\view\html\widget\html\HtmlWidgetItf;
use uroboros\view\html\widget\html\HtmlWidgetStylesheet;
use uroboros\html\HtmlConstant;
use uroboros\localization\alternateTranslation\AlternateTranslation;
use uroboros\localization\alternateTranslation\AlternateTranslationItf;
use uroboros\localization\LanguageCodeConstant;
use uroboros\localization\LanguageNameConstant;

use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetParams;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetParamsItf;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetScript;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetScriptItf;
use blog\localization\translation\BlogTranslationItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use uroboros\mimeType\MimeTypeConstant;
use uroboros\view\html\widget\html\HtmlWidgetFavicon;
use uroboros\view\html\widget\html\HtmlWidgetFaviconItf;

abstract class BlogFrontViewAbs extends HtmlViewAbs
{

	private BlogTranslationContainerItf $translationContainer;
	private BlogFrontBodyWidgetItf $bodyWidget;

	public function __construct(
		HtmlWidgetItf $htmlWidget,
		BlogTranslationContainerItf $translationContainer,
		BlogFrontBodyWidgetItf $bodyWidget
	)
	{
		parent::__construct(
			$htmlWidget
		);
		$this->translationContainer = $translationContainer;
		$this->bodyWidget = $bodyWidget;
	}

	protected function getTranslation():BlogTranslationItf
	{
		return $this->translationContainer->get();
	}

	protected function getTranslationByLanguageCode(string $languageCode):BlogTranslationItf
	{
		return $this->translationContainer->getByLanguageCode($languageCode);
	}

	/**
	 * @return array<string>
	 */
	protected function getHtmlClasses():array
	{
		return [
			'blog-front-html-widget',
		];
	}

	protected function getLanguageCode():string
	{
		return $this->getTranslation()->getLanguageCode();
	}

	protected function getCanonicalHref():string
	{
		return $this->buildAbsoluteUrlByLanguageCode(
			$this->getTranslation()->getLanguageCode()
		);
	}

		abstract protected function buildAbsoluteUrlByLanguageCode(string $languageCode):string;

	protected function getSiteTitle():string
	{
		return $this->getTranslation()->blog_siteTitle();
	}

	protected function renderBodyWidget():string
	{
		return $this->bodyWidget->render(
			$this->buildBodyWidgetParams()
		);
	}

		protected function buildBodyWidgetParams():BlogFrontBodyWidgetParamsItf
		{
			return new BlogFrontBodyWidgetParams(
				$this->getPageTitle(),
				$this->getAlternateTranslations(),
				$this->renderMainBodyWidget(),
				$this->getScripts()
			);
		}

			protected function getPageTitle():string
			{
				return $this->getPageTitleByTranslation(
					$this->getTranslation()
				);
			}
		
				abstract protected function getPageTitleByTranslation(BlogTranslationItf $translation):string;
	
			abstract protected function renderMainBodyWidget():string;

			/**
			 * @return array<BlogFrontBodyWidgetScriptItf>
			 */
			protected function getScripts():array
			{
				return [
					new BlogFrontBodyWidgetScript('asset/blog/zone/front/blogFront.js'),
				];
			}

	protected function getStylesheets():array
	{
		return [
			new HtmlWidgetStylesheet(
				'asset/blog/zone/front/blogFront.css',
				HtmlConstant::ATTRIBUTE_MEDIA_VALUE_SCREEN
			),
		];
	}

	/**
	 * @return array<AlternateTranslationItf>
	 */
	protected function buildAlternateTranslations():array
	{
		$alternateTranslations = [];

		$languages = [
			LanguageCodeConstant::CASTILLIAN => LanguageNameConstant::CASTILLIAN,
			LanguageCodeConstant::CATALAN => LanguageNameConstant::CATALAN,
			LanguageCodeConstant::ENGLISH => LanguageNameConstant::ENGLISH
		];

		foreach($languages as $languageCode => $languageName)
		{
			$alternateTranslations[] = $this->buildAlternateTranslation($languageCode, $languageName);
		}

		return $alternateTranslations;
	}

		protected function buildAlternateTranslation(string $languageCode, string $languageName):AlternateTranslationItf
		{
			$translation = $this->getTranslationByLanguageCode($languageCode);
			$pageTitle = $this->getPageTitleByTranslation($translation);
			return new AlternateTranslation(
				$languageCode,
				$languageName,
				$pageTitle,
				$translation->blog_goTo($pageTitle),
				$this->buildAbsoluteUrlByLanguageCode($languageCode)
			);
		}

	protected function getFavicon():HtmlWidgetFaviconItf
	{
		return new HtmlWidgetFavicon(
			'asset/blog/favicon.png',
			MimeTypeConstant::PNG
		);
	}

}