<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body;

use blog\zone\front\view\widget\body\BlogFrontBodyWidgetParamsItf;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetScriptItf;
use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetAnchorItf;

final class BlogFrontBodyWidgetParams implements BlogFrontBodyWidgetParamsItf
{

	private string $pageTitle;
	/**
	 * @var array<BlogFrontBodyMainHeaderNavWidgetAnchorItf> $anchors
	 */
	private array $anchors;
	private string $mainBody;
	/**
	 * @var array<BlogFrontBodyWidgetScriptItf> $scripts
	 */
	private array $scripts;

	/**
	 * @param array<BlogFrontBodyMainHeaderNavWidgetAnchorItf> $anchors
	 * @param array<BlogFrontBodyWidgetScriptItf> $scripts
	 */
	public function __construct(
		string $pageTitle,
		array $anchors,
		string $mainBody,
		array $scripts
	)
	{
		$this->pageTitle = $pageTitle;
		$this->anchors = $anchors;
		$this->mainBody = $mainBody;
		$this->scripts = $scripts;
	}

	public function getPageTitle():string
	{
		return $this->pageTitle;
	}

	/**
	 * @return array<BlogFrontBodyMainHeaderNavWidgetAnchorItf>
	 */
	public function getAnchors():array
	{
		return $this->anchors;
	}

	public function getMainBody():string
	{
		return $this->mainBody;
	}

	/**
	 * @return array<BlogFrontBodyWidgetScriptItf>
	 */
	public function getScripts():array
	{
		return $this->scripts;
	}

}