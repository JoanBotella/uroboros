<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body;

use blog\zone\front\view\widget\body\BlogFrontBodyWidgetScriptItf;
use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetAnchorItf;

interface BlogFrontBodyWidgetParamsItf
{

	public function getPageTitle():string;

	/**
	 * @return array<BlogFrontBodyMainHeaderNavWidgetAnchorItf>
	 */
	public function getAnchors():array;

	public function getMainBody():string;

	/**
	 * @return array<BlogFrontBodyWidgetScriptItf>
	 */
	public function getScripts():array;

}