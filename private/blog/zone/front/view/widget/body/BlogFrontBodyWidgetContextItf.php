<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body;

use blog\zone\front\view\widget\body\BlogFrontBodyWidgetScriptItf;

interface BlogFrontBodyWidgetContextItf
{

	public function getHeader():string;

	public function getMain():string;

	public function getFooter():string;

	/**
	 * @return array<BlogFrontBodyWidgetScriptItf>
	 */
	public function getScripts():array;

}