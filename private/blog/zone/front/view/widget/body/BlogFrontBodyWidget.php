<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body;

use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetParamsItf;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetContext;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetContextItf;
use blog\zone\front\view\widget\body\widget\footer\BlogFrontBodyFooterWidgetItf;
use blog\zone\front\view\widget\body\widget\footer\BlogFrontBodyFooterWidgetParams;
use blog\zone\front\view\widget\body\widget\footer\BlogFrontBodyFooterWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\header\BlogFrontBodyHeaderWidgetItf;
use blog\zone\front\view\widget\body\widget\header\BlogFrontBodyHeaderWidgetParams;
use blog\zone\front\view\widget\body\widget\header\BlogFrontBodyHeaderWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\main\BlogFrontBodyMainWidgetItf;
use blog\zone\front\view\widget\body\widget\main\BlogFrontBodyMainWidgetParams;
use blog\zone\front\view\widget\body\widget\main\BlogFrontBodyMainWidgetParamsItf;

final class BlogFrontBodyWidget implements BlogFrontBodyWidgetItf
{

	private BlogFrontBodyHeaderWidgetItf $headerWidget;
	private BlogFrontBodyMainWidgetItf $mainWidget;
	private BlogFrontBodyFooterWidgetItf $footerWidget;

	private BlogFrontBodyWidgetParamsItf $params;

	public function __construct(
		BlogFrontBodyHeaderWidgetItf $headerWidget,
		BlogFrontBodyMainWidgetItf $mainWidget,
		BlogFrontBodyFooterWidgetItf $footerWidget
	)
	{
		$this->headerWidget = $headerWidget;
		$this->mainWidget = $mainWidget;
		$this->footerWidget = $footerWidget;
	}

	public function render(BlogFrontBodyWidgetParamsItf $params):string
	{
		$this->params = $params;
		$context = $this->buildContext();
		$r = $this->renderByContext($context);
		unset($this->params);
		return $r;
	}

		private function buildContext():BlogFrontBodyWidgetContextItf
		{
			return new BlogFrontBodyWidgetContext(
				$this->renderHeaderWidget(),
				$this->renderMainWidget(),
				$this->renderFooterWidget(),
				$this->params->getScripts()
			);
		}

			private function renderHeaderWidget():string
			{
				return $this->headerWidget->render(
					$this->buildHeaderWidgetParams()
				);
			}

				private function buildHeaderWidgetParams():BlogFrontBodyHeaderWidgetParamsItf
				{
					return new BlogFrontBodyHeaderWidgetParams();
				}

			private function renderMainWidget():string
			{
				return $this->mainWidget->render(
					$this->buildMainWidgetParams()
				);
			}

				private function buildMainWidgetParams():BlogFrontBodyMainWidgetParamsItf
				{
					return new BlogFrontBodyMainWidgetParams(
						$this->params->getPageTitle(),
						$this->params->getAnchors(),
						$this->params->getMainBody()
					);
				}

			private function renderFooterWidget():string
			{
				return $this->footerWidget->render(
					$this->buildFooterWidgetParams()
				);
			}

				private function buildFooterWidgetParams():BlogFrontBodyFooterWidgetParamsItf
				{
					return new BlogFrontBodyFooterWidgetParams();
				}

		private function renderByContext(BlogFrontBodyWidgetContextItf $context):string
		{
			ob_start();
			require('blogFrontBodyWidgetTemplate.php');
			return ob_get_clean();
		}

}