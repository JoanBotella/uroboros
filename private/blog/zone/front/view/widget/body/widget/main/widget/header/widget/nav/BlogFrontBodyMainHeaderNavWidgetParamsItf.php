<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav;

use uroboros\localization\alternateTranslation\AlternateTranslationItf;

interface BlogFrontBodyMainHeaderNavWidgetParamsItf
{

	/**
	 * @return array<AlternateTranslationItf>
	 */
	public function getAlternateTranslations():array;

}