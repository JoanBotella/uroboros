<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main\widget\header;

use uroboros\localization\alternateTranslation\AlternateTranslationItf;

interface BlogFrontBodyMainHeaderWidgetParamsItf
{

	public function getPageTitle():string;

	/**
	 * @return array<AlternateTranslationItf>
	 */
	public function getAlternateTranslations():array;

}