<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\by\localization\translation;

trait BlogFrontBodyFooterByWidgetTranslationEnTrait
{

	public function blogFrontBodyFooterByWidget_content():string { return 'Powered by <a href="https://framagit.org/JoanBotella/uroboros">Uroboros</a>.'; }

}