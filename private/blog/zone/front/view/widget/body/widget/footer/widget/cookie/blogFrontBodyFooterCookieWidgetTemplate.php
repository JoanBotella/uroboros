<?php

use blog\zone\front\view\widget\body\widget\footer\widget\cookie\BlogFrontBodyFooterCookieWidgetContextItf;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\localization\translation\BlogFrontBodyFooterCookieWidgetTranslationItf;

/**
 * @var BlogFrontBodyFooterCookieWidgetContextItf $context
 * @var BlogFrontBodyFooterCookieWidgetTranslationItf $translation
 */

?>		<div class="blog-front-body-footer-cookie-widget"><?=$translation->blogFrontBodyFooterCookieWidget_content()?></div>
