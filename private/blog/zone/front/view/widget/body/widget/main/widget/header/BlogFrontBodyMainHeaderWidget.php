<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main\widget\header;

use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidgetItf;
use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidgetContext;
use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidgetContextItf;
use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetItf;
use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetParams;
use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetParamsItf;

final class BlogFrontBodyMainHeaderWidget implements BlogFrontBodyMainHeaderWidgetItf
{

	private BlogFrontBodyMainHeaderNavWidgetItf $navWidget;
	private BlogFrontBodyMainHeaderWidgetParamsItf $params;

	public function __construct(
		BlogFrontBodyMainHeaderNavWidgetItf $navWidget
	)
	{
		$this->navWidget = $navWidget;
	}

	public function render(BlogFrontBodyMainHeaderWidgetParamsItf $params):string
	{
		$this->params = $params;
		$context = $this->buildContext();
		$r = $this->renderByContext($context);
		unset($this->params);
		return $r;
	}

		private function buildContext():BlogFrontBodyMainHeaderWidgetContextItf
		{
			return new BlogFrontBodyMainHeaderWidgetContext(
				$this->params->getPageTitle(),
				$this->renderMainHeaderNavWidget()
			);
		}

			private function renderMainHeaderNavWidget():string
			{
				return $this->navWidget->render(
					$this->buildMainHeaderNavWidgetParams()
				);
			}

				private function buildMainHeaderNavWidgetParams():BlogFrontBodyMainHeaderNavWidgetParamsItf
				{
					return new BlogFrontBodyMainHeaderNavWidgetParams(
						$this->params->getAlternateTranslations()
					);
				}

		private function renderByContext(BlogFrontBodyMainHeaderWidgetContextItf $context):string
		{
			ob_start();
			require('blogFrontBodyMainHeaderWidgetTemplate.php');
			return ob_get_clean();
		}

}