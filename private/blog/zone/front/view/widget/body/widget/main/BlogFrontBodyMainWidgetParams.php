<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main;

use blog\zone\front\view\widget\body\widget\main\BlogFrontBodyMainWidgetParamsItf;
use uroboros\localization\alternateTranslation\AlternateTranslationItf;

final class BlogFrontBodyMainWidgetParams implements BlogFrontBodyMainWidgetParamsItf
{

	private string $pageTitle;
	/**
	 * @var array<AlternateTranslationItf> $alternateTranslations
	 */
	private array $alternateTranslations;
	private string $body;

	/**
	 * @param array<AlternateTranslationItf> $alternateTranslations
	 */
	public function __construct(
		string $pageTitle,
		array $alternateTranslations,
		string $body
	)
	{
		$this->pageTitle = $pageTitle;
		$this->alternateTranslations = $alternateTranslations;
		$this->body = $body;
	}

	public function getPageTitle():string
	{
		return $this->pageTitle;
	}

	/**
	 * @return array<AlternateTranslationItf>
	 */
	public function getAlternateTranslations():array
	{
		return $this->alternateTranslations;
	}

	public function getBody():string
	{
		return $this->body;
	}

}