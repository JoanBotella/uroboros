<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\cookie;

use blog\zone\front\view\widget\body\widget\footer\widget\cookie\BlogFrontBodyFooterCookieWidgetParamsItf;

interface BlogFrontBodyFooterCookieWidgetItf
{

	public function render(BlogFrontBodyFooterCookieWidgetParamsItf $params):string;

}