<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\header\widget\nav;

use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetAnchorItf;

interface BlogFrontBodyHeaderNavWidgetContextItf
{

	/**
	 * @return array<BlogFrontBodyHeaderNavWidgetAnchorItf>
	 */
	public function getAnchors():array;

}