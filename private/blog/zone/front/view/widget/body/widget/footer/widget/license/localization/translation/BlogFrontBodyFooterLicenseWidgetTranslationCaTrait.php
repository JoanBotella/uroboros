<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\license\localization\translation;

trait BlogFrontBodyFooterLicenseWidgetTranslationCaTrait
{

	public function blogFrontBodyFooterLicenseWidget_content():string { return 'Llevat que s\'indique el contrari, tot el contingut d\'esta web està sota una <a href="https://creativecommons.org/licenses/by-nc-sa/4.0">llicència CC BY-NC-SA 4.0</a>.'; }

}