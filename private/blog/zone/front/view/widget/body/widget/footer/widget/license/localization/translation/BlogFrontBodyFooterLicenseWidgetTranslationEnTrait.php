<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\license\localization\translation;

trait BlogFrontBodyFooterLicenseWidgetTranslationEnTrait
{

	public function blogFrontBodyFooterLicenseWidget_content():string { return 'Unless otherwise stated, all content of this website is under a <a href="https://creativecommons.org/licenses/by-nc-sa/4.0">CC BY-NC-SA 4.0 license</a>.'; }

}