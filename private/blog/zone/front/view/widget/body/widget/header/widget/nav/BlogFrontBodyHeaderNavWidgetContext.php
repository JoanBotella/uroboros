<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\header\widget\nav;

use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetContextItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetAnchorItf;

final class BlogFrontBodyHeaderNavWidgetContext implements BlogFrontBodyHeaderNavWidgetContextItf
{

	/**
	 * @param array<BlogFrontBodyHeaderNavWidgetAnchorItf> $anchors
	 */
	private array $anchors;

	/**
	 * @param array<BlogFrontBodyHeaderNavWidgetAnchorItf> $anchors
	 */
	public function __construct(
		array $anchors
	)
	{
		$this->anchors = $anchors;
	}

	/**
	 * @return array<BlogFrontBodyHeaderNavWidgetAnchorItf>
	 */
	public function getAnchors():array
	{
		return $this->anchors;
	}

}