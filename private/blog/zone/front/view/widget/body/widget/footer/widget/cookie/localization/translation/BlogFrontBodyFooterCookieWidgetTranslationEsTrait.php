<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\cookie\localization\translation;

trait BlogFrontBodyFooterCookieWidgetTranslationEsTrait
{

	public function blogFrontBodyFooterCookieWidget_content():string { return 'Esta web <strong>no</strong> utiliza cookies.<br />Joan se las ha comido todas.'; }

}