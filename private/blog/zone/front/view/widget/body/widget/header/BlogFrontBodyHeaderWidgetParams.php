<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\header;

use blog\zone\front\view\widget\body\widget\header\BlogFrontBodyHeaderWidgetParamsItf;

final class BlogFrontBodyHeaderWidgetParams implements BlogFrontBodyHeaderWidgetParamsItf
{
}