<?php

use blog\zone\front\view\widget\body\widget\footer\widget\license\BlogFrontBodyFooterLicenseWidgetContextItf;
use blog\zone\front\view\widget\body\widget\footer\widget\license\localization\translation\BlogFrontBodyFooterLicenseWidgetTranslationItf;

/**
 * @var BlogFrontBodyFooterLicenseWidgetContextItf $context
 * @var BlogFrontBodyFooterLicenseWidgetTranslationItf $translation
 */

?>		<div class="blog-front-body-footer-license-widget"><?=$translation->blogFrontBodyFooterLicenseWidget_content()?></div>
