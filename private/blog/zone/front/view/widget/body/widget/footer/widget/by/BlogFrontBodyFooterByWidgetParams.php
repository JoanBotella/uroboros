<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\by;

use blog\zone\front\view\widget\body\widget\footer\widget\by\BlogFrontBodyFooterByWidgetParamsItf;

final class BlogFrontBodyFooterByWidgetParams implements BlogFrontBodyFooterByWidgetParamsItf
{
}