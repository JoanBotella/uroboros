<?php

use blog\zone\front\view\widget\body\widget\header\BlogFrontBodyHeaderWidgetContextItf;
use blog\localization\translation\BlogTranslationItf;

/**
 * @var BlogFrontBodyHeaderWidgetContextItf $context
 * @var BlogTranslationItf $translation
 */

?><header class="blog-front-body-header-widget">
	<a href="<?=$context->getHomeUrl()?>"><?=$translation->blog_siteTitle()?></a>

<?=$context->getNav()?>
</header>
