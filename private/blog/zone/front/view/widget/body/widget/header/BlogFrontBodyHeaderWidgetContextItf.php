<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\header;

interface BlogFrontBodyHeaderWidgetContextItf
{

	public function getHomeUrl():string;

	public function getNav():string;

}