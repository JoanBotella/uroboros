<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\localization\translation;

use blog\zone\front\view\widget\body\widget\footer\widget\by\localization\translation\BlogFrontBodyFooterByWidgetTranslationEnTrait;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\localization\translation\BlogFrontBodyFooterCookieWidgetTranslationEnTrait;
use blog\zone\front\view\widget\body\widget\footer\widget\license\localization\translation\BlogFrontBodyFooterLicenseWidgetTranslationEnTrait;

trait BlogFrontBodyFooterWidgetTranslationEnTrait
{
	use
		BlogFrontBodyFooterCookieWidgetTranslationEnTrait,
		BlogFrontBodyFooterLicenseWidgetTranslationEnTrait,
		BlogFrontBodyFooterByWidgetTranslationEnTrait
	;
}