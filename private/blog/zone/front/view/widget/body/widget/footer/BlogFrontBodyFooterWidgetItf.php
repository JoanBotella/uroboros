<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer;

use blog\zone\front\view\widget\body\widget\footer\BlogFrontBodyFooterWidgetParamsItf;

interface BlogFrontBodyFooterWidgetItf
{

	public function render(BlogFrontBodyFooterWidgetParamsItf $params):string;

}