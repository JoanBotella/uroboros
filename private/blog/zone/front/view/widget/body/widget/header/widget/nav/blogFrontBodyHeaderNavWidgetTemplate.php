<?php

use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetContextItf;

/**
 * @var BlogFrontBodyHeaderNavWidgetContextItf $context
 */

?>		<nav class="blog-front-body-header-nav-widget">
<?php foreach($context->getAnchors() as $anchor) { ?>
			<a title="<?=$anchor->getTitle()?>" href="<?=$anchor->getHref()?>"><?=$anchor->getContent()?></a>
<?php } ?>
		</nav>
