<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main\widget\header;

use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidgetParamsItf;
use uroboros\localization\alternateTranslation\AlternateTranslationItf;

final class BlogFrontBodyMainHeaderWidgetParams implements BlogFrontBodyMainHeaderWidgetParamsItf
{

	private string $pageTitle;
	/**
	 * @var array<AlternateTranslationItf> $alternateTranslations
	 */
	private array $alternateTranslations;

	/**
	 * @param array<AlternateTranslationItf> $alternateTranslations
	 */
	public function __construct(
		string $pageTitle,
		array $alternateTranslations
	)
	{
		$this->pageTitle = $pageTitle;
		$this->alternateTranslations = $alternateTranslations;
	}

	public function getPageTitle():string
	{
		return $this->pageTitle;
	}

	/**
	 * @return array<AlternateTranslationItf>
	 */
	public function getAlternateTranslations():array
	{
		return $this->alternateTranslations;
	}

}