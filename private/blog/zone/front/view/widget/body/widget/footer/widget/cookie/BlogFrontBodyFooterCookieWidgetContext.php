<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\cookie;

use blog\zone\front\view\widget\body\widget\footer\widget\cookie\BlogFrontBodyFooterCookieWidgetContextItf;

final class BlogFrontBodyFooterCookieWidgetContext implements BlogFrontBodyFooterCookieWidgetContextItf
{
}