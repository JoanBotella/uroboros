<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\cookie\localization\translation;

interface BlogFrontBodyFooterCookieWidgetTranslationItf
{

	public function blogFrontBodyFooterCookieWidget_content():string;

}