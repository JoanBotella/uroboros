<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\cookie\localization\translation;

trait BlogFrontBodyFooterCookieWidgetTranslationCaTrait
{

	public function blogFrontBodyFooterCookieWidget_content():string { return 'Esta web <strong>no</strong> utilitza cookies.<br />Joan se les ha menjat totes.'; }

}