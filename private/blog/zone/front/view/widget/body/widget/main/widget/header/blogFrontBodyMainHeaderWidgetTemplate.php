<?php

use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidgetContextItf;

/**
 * @var BlogFrontBodyMainHeaderWidgetContextItf $context
 */

?>		<header class="blog-front-body-main-header-widget">
			<h1><?=$context->getH1Content()?></h1>

<?=$context->getNav()?>
		</header>