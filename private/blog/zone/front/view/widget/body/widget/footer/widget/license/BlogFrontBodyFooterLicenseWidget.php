<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\license;

use blog\widget\BlogTranslatableWidgetAbs;
use blog\zone\front\view\widget\body\widget\footer\widget\license\BlogFrontBodyFooterLicenseWidgetItf;
use blog\zone\front\view\widget\body\widget\footer\widget\license\BlogFrontBodyFooterLicenseWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\footer\widget\license\BlogFrontBodyFooterLicenseWidgetContext;
use blog\zone\front\view\widget\body\widget\footer\widget\license\BlogFrontBodyFooterLicenseWidgetContextItf;

final class BlogFrontBodyFooterLicenseWidget extends BlogTranslatableWidgetAbs implements BlogFrontBodyFooterLicenseWidgetItf
{

	public function render(BlogFrontBodyFooterLicenseWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(BlogFrontBodyFooterLicenseWidgetParamsItf $params):BlogFrontBodyFooterLicenseWidgetContextItf
		{
			return new BlogFrontBodyFooterLicenseWidgetContext();
		}

		private function renderByContext(BlogFrontBodyFooterLicenseWidgetContextItf $context):string
		{
			$translation = $this->getTranslation();
			ob_start();
			require('blogFrontBodyFooterLicenseWidgetTemplate.php');
			return ob_get_clean();
		}

}