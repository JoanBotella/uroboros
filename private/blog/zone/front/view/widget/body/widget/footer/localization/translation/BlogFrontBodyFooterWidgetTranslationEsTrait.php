<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\localization\translation;

use blog\zone\front\view\widget\body\widget\footer\widget\by\localization\translation\BlogFrontBodyFooterByWidgetTranslationEsTrait;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\localization\translation\BlogFrontBodyFooterCookieWidgetTranslationEsTrait;
use blog\zone\front\view\widget\body\widget\footer\widget\license\localization\translation\BlogFrontBodyFooterLicenseWidgetTranslationEsTrait;

trait BlogFrontBodyFooterWidgetTranslationEsTrait
{
	use
		BlogFrontBodyFooterCookieWidgetTranslationEsTrait,
		BlogFrontBodyFooterLicenseWidgetTranslationEsTrait,
		BlogFrontBodyFooterByWidgetTranslationEsTrait
	;
}