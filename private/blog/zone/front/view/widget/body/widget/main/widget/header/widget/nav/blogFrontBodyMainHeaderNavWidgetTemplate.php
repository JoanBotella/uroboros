<?php

use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetContextItf;

/**
 * @var BlogFrontBodyMainHeaderNavWidgetContextItf $context
 */

?>		<nav class="blog-front-body-main-header-nav-widget">
<?php foreach($context->getAlternateTranslations() as $anchor) { ?>
		<a title="<?=$anchor->getAnchorTitle()?>" href="<?=$anchor->getUrl()?>" lang="<?=$anchor->getLanguageCode()?>" hreflang="<?=$anchor->getLanguageCode()?>"><?=$anchor->getLanguageName()?></a>
<?php } ?>
	</nav>
