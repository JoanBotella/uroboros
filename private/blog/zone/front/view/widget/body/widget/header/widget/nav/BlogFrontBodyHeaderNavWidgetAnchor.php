<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\header\widget\nav;

use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetAnchorItf;

final class BlogFrontBodyHeaderNavWidgetAnchor implements BlogFrontBodyHeaderNavWidgetAnchorItf
{

	private string $href;
	private string $title;
	private string $content;

	public function __construct(
		string $href,
		string $title,
		string $content
	)
	{
		$this->href = $href;
		$this->title = $title;
		$this->content = $content;
	}

	public function getHref():string
	{
		return $this->href;
	}

	public function getTitle():string
	{
		return $this->title;
	}

	public function getContent():string
	{
		return $this->content;
	}

}