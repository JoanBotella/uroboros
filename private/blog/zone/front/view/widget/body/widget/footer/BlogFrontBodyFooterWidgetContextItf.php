<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer;

interface BlogFrontBodyFooterWidgetContextItf
{

	public function getCookie():string;

	public function getLicense():string;

	public function getBy():string;

}