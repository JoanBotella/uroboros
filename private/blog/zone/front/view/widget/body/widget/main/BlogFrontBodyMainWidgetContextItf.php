<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main;

interface BlogFrontBodyMainWidgetContextItf
{

	public function getHeader():string;

	public function getBody():string;

}