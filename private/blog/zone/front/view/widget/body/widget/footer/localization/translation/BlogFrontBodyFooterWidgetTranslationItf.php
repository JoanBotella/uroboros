<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\localization\translation;

use blog\zone\front\view\widget\body\widget\footer\widget\by\localization\translation\BlogFrontBodyFooterByWidgetTranslationItf;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\localization\translation\BlogFrontBodyFooterCookieWidgetTranslationItf;
use blog\zone\front\view\widget\body\widget\footer\widget\license\localization\translation\BlogFrontBodyFooterLicenseWidgetTranslationItf;

interface BlogFrontBodyFooterWidgetTranslationItf
extends
	BlogFrontBodyFooterCookieWidgetTranslationItf,
	BlogFrontBodyFooterLicenseWidgetTranslationItf,
	BlogFrontBodyFooterByWidgetTranslationItf
{
}
