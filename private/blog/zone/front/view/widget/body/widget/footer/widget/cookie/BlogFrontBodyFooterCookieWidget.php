<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\cookie;

use blog\widget\BlogTranslatableWidgetAbs;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\BlogFrontBodyFooterCookieWidgetItf;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\BlogFrontBodyFooterCookieWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\BlogFrontBodyFooterCookieWidgetContext;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\BlogFrontBodyFooterCookieWidgetContextItf;

final class BlogFrontBodyFooterCookieWidget extends BlogTranslatableWidgetAbs implements BlogFrontBodyFooterCookieWidgetItf
{

	public function render(BlogFrontBodyFooterCookieWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(BlogFrontBodyFooterCookieWidgetParamsItf $params):BlogFrontBodyFooterCookieWidgetContextItf
		{
			return new BlogFrontBodyFooterCookieWidgetContext();
		}

		private function renderByContext(BlogFrontBodyFooterCookieWidgetContextItf $context):string
		{
			$translation = $this->getTranslation();
			ob_start();
			require('blogFrontBodyFooterCookieWidgetTemplate.php');
			return ob_get_clean();
		}

}