<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\license\localization\translation;

interface BlogFrontBodyFooterLicenseWidgetTranslationItf
{

	public function blogFrontBodyFooterLicenseWidget_content():string;

}