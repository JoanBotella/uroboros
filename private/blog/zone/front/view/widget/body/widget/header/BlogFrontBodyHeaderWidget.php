<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\header;

use blog\zone\front\page\home\urlBuilder\BlogFrontHomeUrlBuilderItf;
use blog\zone\front\view\widget\body\widget\header\BlogFrontBodyHeaderWidgetItf;
use blog\zone\front\view\widget\body\widget\header\BlogFrontBodyHeaderWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\header\BlogFrontBodyHeaderWidgetContext;
use blog\zone\front\view\widget\body\widget\header\BlogFrontBodyHeaderWidgetContextItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetParams;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetParamsItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use blog\widget\BlogTranslatableWidgetAbs;

final class BlogFrontBodyHeaderWidget extends BlogTranslatableWidgetAbs implements BlogFrontBodyHeaderWidgetItf
{

	private BlogFrontHomeUrlBuilderItf $homeUrlBuilder;
	private BlogFrontBodyHeaderNavWidgetItf $navWidget;

	public function __construct(
		BlogTranslationContainerItf $translationContainer,
		BlogFrontHomeUrlBuilderItf $homeUrlBuilder,
		BlogFrontBodyHeaderNavWidgetItf $navWidget
	)
	{
		parent::__construct(
			$translationContainer
		);
		$this->homeUrlBuilder = $homeUrlBuilder;
		$this->navWidget = $navWidget;
	}

	public function render(BlogFrontBodyHeaderWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(BlogFrontBodyHeaderWidgetParamsItf $params):BlogFrontBodyHeaderWidgetContextItf
		{
			return new BlogFrontBodyHeaderWidgetContext(
				$this->buildHomeUrl(),
				$this->renderNavWidget()
			);
		}

			private function buildHomeUrl():string
			{
				return $this->homeUrlBuilder->buildRelative(
					$this->getLanguageCode()
				);
			}

			private function renderNavWidget():string
			{
				return $this->navWidget->render(
					$this->buildNavWidgetParams()
				);
			}

				private function buildNavWidgetParams():BlogFrontBodyHeaderNavWidgetParamsItf
				{
					return new BlogFrontBodyHeaderNavWidgetParams();
				}

		private function renderByContext(BlogFrontBodyHeaderWidgetContextItf $context):string
		{
			$translation = $this->getTranslation();
			ob_start();
			require('blogFrontBodyHeaderWidgetTemplate.php');
			return ob_get_clean();
		}

}