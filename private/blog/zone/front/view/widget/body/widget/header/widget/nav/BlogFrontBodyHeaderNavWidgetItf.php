<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\header\widget\nav;

use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetParamsItf;

interface BlogFrontBodyHeaderNavWidgetItf
{

	public function render(BlogFrontBodyHeaderNavWidgetParamsItf $params):string;

}