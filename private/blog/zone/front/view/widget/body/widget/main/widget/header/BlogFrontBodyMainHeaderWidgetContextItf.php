<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main\widget\header;

interface BlogFrontBodyMainHeaderWidgetContextItf
{

	public function getH1Content():string;

	public function getNav():string;

}