<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\by\localization\translation;

interface BlogFrontBodyFooterByWidgetTranslationItf
{

	public function blogFrontBodyFooterByWidget_content():string;

}