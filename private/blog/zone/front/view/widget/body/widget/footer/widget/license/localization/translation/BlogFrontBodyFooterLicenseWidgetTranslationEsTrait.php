<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\license\localization\translation;

trait BlogFrontBodyFooterLicenseWidgetTranslationEsTrait
{

	public function blogFrontBodyFooterLicenseWidget_content():string { return 'A menos que se indique lo contrario, todo el contenido de esta web está bajo una <a href="https://creativecommons.org/licenses/by-nc-sa/4.0">licencia CC BY-NC-SA 4.0</a>.'; }

}