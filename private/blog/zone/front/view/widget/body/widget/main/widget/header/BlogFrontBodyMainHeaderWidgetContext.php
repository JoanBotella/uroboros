<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main\widget\header;

use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidgetContextItf;

final class BlogFrontBodyMainHeaderWidgetContext implements BlogFrontBodyMainHeaderWidgetContextItf
{

	private string $h1Content;
	private string $nav;

	public function __construct(
		string $h1Content,
		string $nav
	)
	{
		$this->h1Content = $h1Content;
		$this->nav = $nav;
	}

	public function getH1Content():string
	{
		return $this->h1Content;
	}

	public function getNav():string
	{
		return $this->nav;
	}

}