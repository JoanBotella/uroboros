<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main;

use blog\zone\front\view\widget\body\widget\main\BlogFrontBodyMainWidgetItf;
use blog\zone\front\view\widget\body\widget\main\BlogFrontBodyMainWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\main\BlogFrontBodyMainWidgetContextItf;
use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidgetItf;
use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidgetParams;
use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidgetParamsItf;

final class BlogFrontBodyMainWidget implements BlogFrontBodyMainWidgetItf
{

	private BlogFrontBodyMainHeaderWidgetItf $headerWidget;

	private BlogFrontBodyMainWidgetParamsItf $params;

	public function __construct(
		BlogFrontBodyMainHeaderWidgetItf $headerWidget
	)
	{
		$this->headerWidget = $headerWidget;
	}

	public function render(BlogFrontBodyMainWidgetParamsItf $params):string
	{
		$this->params = $params;
		$context = $this->buildContext();
		$r = $this->renderByContext($context);
		unset($this->params);
		return $r;
	}

		private function buildContext():BlogFrontBodyMainWidgetContextItf
		{
			return new BlogFrontBodyMainWidgetContext(
				$this->renderHeaderWidget(),
				$this->params->getBody()
			);
		}

			private function renderHeaderWidget():string
			{
				return $this->headerWidget->render(
					$this->buildHeaderWidgetParams()
				);
			}

				private function buildHeaderWidgetParams():BlogFrontBodyMainHeaderWidgetParamsItf
				{
					return new BlogFrontBodyMainHeaderWidgetParams(
						$this->params->getPageTitle(),
						$this->params->getAlternateTranslations()
					);
				}

		private function renderByContext(BlogFrontBodyMainWidgetContextItf $context):string
		{
			ob_start();
			require('blogFrontBodyMainWidgetTemplate.php');
			return ob_get_clean();
		}

}
