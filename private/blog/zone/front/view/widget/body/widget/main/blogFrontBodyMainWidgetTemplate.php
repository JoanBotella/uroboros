<?php

use blog\zone\front\page\home\view\widget\main\BlogFrontNotFoundMainWidgetContextItf;

/**
 * @var BlogFrontNotFoundMainWidgetContextItf $context
 */

?>	<main class="blog-front-body-main-widget">
<?=$context->getHeader()?>

		<div class="blog-front-body-main-widget__body">
<?=$context->getBody()?>
		</div>

	</main>
