<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\cookie\localization\translation;

trait BlogFrontBodyFooterCookieWidgetTranslationEnTrait
{

	public function blogFrontBodyFooterCookieWidget_content():string { return 'This website does <strong>not</strong> use cookies.<br />Joan ate them all.'; }

}