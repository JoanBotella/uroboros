<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer;

use blog\zone\front\view\widget\body\widget\footer\BlogFrontBodyFooterWidgetContextItf;

final class BlogFrontBodyFooterWidgetContext implements BlogFrontBodyFooterWidgetContextItf
{

	private string $cookie;
	private string $license;
	private string $by;

	public function __construct(
		string $cookie,
		string $license,
		string $by
	)
	{
		$this->cookie = $cookie;
		$this->license = $license;
		$this->by = $by;
	}

	public function getCookie():string
	{
		return $this->cookie;
	}

	public function getLicense():string
	{
		return $this->license;
	}

	public function getBy():string
	{
		return $this->by;
	}

}