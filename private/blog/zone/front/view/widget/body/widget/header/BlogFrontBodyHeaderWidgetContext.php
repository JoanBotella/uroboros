<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\header;

use blog\zone\front\view\widget\body\widget\header\BlogFrontBodyHeaderWidgetContextItf;

final class BlogFrontBodyHeaderWidgetContext implements BlogFrontBodyHeaderWidgetContextItf
{

	private string $homeUrl;
	private string $nav;

	public function __construct(
		string $homeUrl,
		string $nav
	)
	{
		$this->homeUrl = $homeUrl;
		$this->nav = $nav;
	}

	public function getHomeUrl():string
	{
		return $this->homeUrl;
	}

	public function getNav():string
	{
		return $this->nav;
	}

}