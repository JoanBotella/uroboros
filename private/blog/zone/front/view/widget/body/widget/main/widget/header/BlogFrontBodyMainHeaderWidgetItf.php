<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main\widget\header;

use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidgetParamsItf;

interface BlogFrontBodyMainHeaderWidgetItf
{

	public function render(BlogFrontBodyMainHeaderWidgetParamsItf $params):string;

}