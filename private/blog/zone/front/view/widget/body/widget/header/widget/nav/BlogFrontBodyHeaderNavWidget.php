<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\header\widget\nav;

use blog\zone\front\page\aboutMe\urlBuilder\BlogFrontAboutMeUrlBuilderItf;
use blog\zone\front\page\aboutBlog\urlBuilder\BlogFrontAboutBlogUrlBuilderItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetContext;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetContextItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetAnchor;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetAnchorItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use blog\widget\BlogTranslatableWidgetAbs;
use blog\zone\front\page\contact\urlBuilder\BlogFrontContactUrlBuilderItf;

final class BlogFrontBodyHeaderNavWidget extends BlogTranslatableWidgetAbs implements BlogFrontBodyHeaderNavWidgetItf
{

	private BlogFrontAboutMeUrlBuilderItf $frontAboutMeUrlBuilder;
	private BlogFrontAboutBlogUrlBuilderItf $frontAboutBlogUrlBuilder;
	private BlogFrontContactUrlBuilderItf $frontContactUrlBuilder;

	public function __construct(
		BlogTranslationContainerItf $translationContainer,
		BlogFrontAboutMeUrlBuilderItf $frontAboutMeUrlBuilder,
		BlogFrontAboutBlogUrlBuilderItf $frontAboutBlogUrlBuilder,
		BlogFrontContactUrlBuilderItf $frontContactUrlBuilder
	)
	{
		parent::__construct(
			$translationContainer
		);
		$this->frontAboutMeUrlBuilder = $frontAboutMeUrlBuilder;
		$this->frontAboutBlogUrlBuilder = $frontAboutBlogUrlBuilder;
		$this->frontContactUrlBuilder = $frontContactUrlBuilder;
	}

	public function render(BlogFrontBodyHeaderNavWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(BlogFrontBodyHeaderNavWidgetParamsItf $params):BlogFrontBodyHeaderNavWidgetContextItf
		{
			return new BlogFrontBodyHeaderNavWidgetContext(
				$this->buildAnchors()
			);
		}

			/**
			 * @return array<BlogFrontBodyHeaderNavWidgetAnchorItf>
			 */
			private function buildAnchors():array
			{
				return [
					$this->buildAboutMeAnchor(),
					$this->buildAboutBlogAnchor(),
					$this->buildContactAnchor(),
				];
			}

				private function buildAboutMeAnchor():BlogFrontBodyHeaderNavWidgetAnchorItf
				{
					$translation = $this->getTranslation();
					$pageTitle = $translation->blogFrontAboutMe_pageTitle();
					return new BlogFrontBodyHeaderNavWidgetAnchor(
						$this->frontAboutMeUrlBuilder->buildRelative(
							$translation->getLanguageCode()
						),
						$translation->blog_goTo($pageTitle),
						$pageTitle
					);
				}

				private function buildAboutBlogAnchor():BlogFrontBodyHeaderNavWidgetAnchorItf
				{
					$translation = $this->getTranslation();
					$pageTitle = $translation->blogFrontAboutBlog_pageTitle();
					return new BlogFrontBodyHeaderNavWidgetAnchor(
						$this->frontAboutBlogUrlBuilder->buildRelative(
							$translation->getLanguageCode()
						),
						$translation->blog_goTo($pageTitle),
						$pageTitle
					);
				}

				private function buildContactAnchor():BlogFrontBodyHeaderNavWidgetAnchorItf
				{
					$translation = $this->getTranslation();
					$pageTitle = $translation->blogFrontContact_pageTitle();
					return new BlogFrontBodyHeaderNavWidgetAnchor(
						$this->frontContactUrlBuilder->buildRelative(
							$translation->getLanguageCode()
						),
						$translation->blog_goTo($pageTitle),
						$pageTitle
					);
				}
		private function renderByContext(BlogFrontBodyHeaderNavWidgetContextItf $context):string
		{
			ob_start();
			require('blogFrontBodyHeaderNavWidgetTemplate.php');
			return ob_get_clean();
		}

}