<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav;

use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetParamsItf;

interface BlogFrontBodyMainHeaderNavWidgetItf
{

	public function render(BlogFrontBodyMainHeaderNavWidgetParamsItf $params):string;

}