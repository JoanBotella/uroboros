<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main;

use uroboros\localization\alternateTranslation\AlternateTranslationItf;

interface BlogFrontBodyMainWidgetParamsItf
{

	public function getPageTitle():string;

	/**
	 * @return array<AlternateTranslationItf>
	 */
	public function getAlternateTranslations():array;

	public function getBody():string;

}