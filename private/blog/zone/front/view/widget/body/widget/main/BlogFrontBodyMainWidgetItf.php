<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main;

use blog\zone\front\view\widget\body\widget\main\BlogFrontBodyMainWidgetParamsItf;

interface BlogFrontBodyMainWidgetItf
{

	public function render(BlogFrontBodyMainWidgetParamsItf $params):string;

}
