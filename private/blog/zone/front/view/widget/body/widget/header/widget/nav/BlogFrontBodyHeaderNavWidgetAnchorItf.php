<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\header\widget\nav;

interface BlogFrontBodyHeaderNavWidgetAnchorItf
{

	public function getHref():string;

	public function getTitle():string;

	public function getContent():string;

}