<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\license;

use blog\zone\front\view\widget\body\widget\footer\widget\license\BlogFrontBodyFooterLicenseWidgetContextItf;

final class BlogFrontBodyFooterLicenseWidgetContext implements BlogFrontBodyFooterLicenseWidgetContextItf
{
}