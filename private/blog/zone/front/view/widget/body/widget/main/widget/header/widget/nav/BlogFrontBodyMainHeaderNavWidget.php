<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav;

use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetItf;
use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetContext;
use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetContextItf;

final class BlogFrontBodyMainHeaderNavWidget implements BlogFrontBodyMainHeaderNavWidgetItf
{

	public function render(BlogFrontBodyMainHeaderNavWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(BlogFrontBodyMainHeaderNavWidgetParamsItf $params):BlogFrontBodyMainHeaderNavWidgetContextItf
		{
			return new BlogFrontBodyMainHeaderNavWidgetContext(
				$params->getAlternateTranslations()
			);
		}

		private function renderByContext(BlogFrontBodyMainHeaderNavWidgetContextItf $context):string
		{
			ob_start();
			require('blogFrontBodyMainHeaderNavWidgetTemplate.php');
			return ob_get_clean();
		}

}