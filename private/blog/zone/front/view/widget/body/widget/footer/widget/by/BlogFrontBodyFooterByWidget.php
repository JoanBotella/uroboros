<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\widget\by;

use blog\widget\BlogTranslatableWidgetAbs;
use blog\zone\front\view\widget\body\widget\footer\widget\by\BlogFrontBodyFooterByWidgetItf;
use blog\zone\front\view\widget\body\widget\footer\widget\by\BlogFrontBodyFooterByWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\footer\widget\by\BlogFrontBodyFooterByWidgetContext;
use blog\zone\front\view\widget\body\widget\footer\widget\by\BlogFrontBodyFooterByWidgetContextItf;

final class BlogFrontBodyFooterByWidget extends BlogTranslatableWidgetAbs implements BlogFrontBodyFooterByWidgetItf
{

	public function render(BlogFrontBodyFooterByWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(BlogFrontBodyFooterByWidgetParamsItf $params):BlogFrontBodyFooterByWidgetContextItf
		{
			return new BlogFrontBodyFooterByWidgetContext();
		}

		private function renderByContext(BlogFrontBodyFooterByWidgetContextItf $context):string
		{
			$translation = $this->getTranslation();
			ob_start();
			require('blogFrontBodyFooterByWidgetTemplate.php');
			return ob_get_clean();
		}

}