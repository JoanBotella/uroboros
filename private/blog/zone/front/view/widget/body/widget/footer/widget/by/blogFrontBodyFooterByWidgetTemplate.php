<?php

use blog\zone\front\view\widget\body\widget\footer\widget\by\BlogFrontBodyFooterByWidgetContextItf;
use blog\zone\front\view\widget\body\widget\footer\widget\by\localization\translation\BlogFrontBodyFooterByWidgetTranslationItf;

/**
 * @var BlogFrontBodyFooterByWidgetContextItf $context
 * @var BlogFrontBodyFooterByWidgetTranslationItf $translation
 */

?>		<div class="blog-front-body-footer-by-widget"><?=$translation->blogFrontBodyFooterByWidget_content()?></div>
