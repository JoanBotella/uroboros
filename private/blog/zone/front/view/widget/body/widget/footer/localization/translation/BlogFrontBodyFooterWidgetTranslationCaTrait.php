<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer\localization\translation;

use blog\zone\front\view\widget\body\widget\footer\widget\by\localization\translation\BlogFrontBodyFooterByWidgetTranslationCaTrait;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\localization\translation\BlogFrontBodyFooterCookieWidgetTranslationCaTrait;
use blog\zone\front\view\widget\body\widget\footer\widget\license\localization\translation\BlogFrontBodyFooterLicenseWidgetTranslationCaTrait;

trait BlogFrontBodyFooterWidgetTranslationCaTrait
{
	use
		BlogFrontBodyFooterCookieWidgetTranslationCaTrait,
		BlogFrontBodyFooterLicenseWidgetTranslationCaTrait,
		BlogFrontBodyFooterByWidgetTranslationCaTrait
	;
}