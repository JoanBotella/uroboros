<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\footer;

use blog\zone\front\view\widget\body\widget\footer\widget\by\BlogFrontBodyFooterByWidgetItf;
use blog\zone\front\view\widget\body\widget\footer\widget\by\BlogFrontBodyFooterByWidgetParams;
use blog\zone\front\view\widget\body\widget\footer\widget\by\BlogFrontBodyFooterByWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\BlogFrontBodyFooterCookieWidgetItf;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\BlogFrontBodyFooterCookieWidgetParams;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\BlogFrontBodyFooterCookieWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\footer\BlogFrontBodyFooterWidgetItf;
use blog\zone\front\view\widget\body\widget\footer\BlogFrontBodyFooterWidgetParamsItf;
use blog\zone\front\view\widget\body\widget\footer\BlogFrontBodyFooterWidgetContext;
use blog\zone\front\view\widget\body\widget\footer\BlogFrontBodyFooterWidgetContextItf;
use blog\zone\front\view\widget\body\widget\footer\widget\license\BlogFrontBodyFooterLicenseWidgetItf;
use blog\zone\front\view\widget\body\widget\footer\widget\license\BlogFrontBodyFooterLicenseWidgetParams;
use blog\zone\front\view\widget\body\widget\footer\widget\license\BlogFrontBodyFooterLicenseWidgetParamsItf;

final class BlogFrontBodyFooterWidget implements BlogFrontBodyFooterWidgetItf
{

	private BlogFrontBodyFooterCookieWidgetItf $cookieWidget;
	private BlogFrontBodyFooterLicenseWidgetItf $licenseWidget;
	private BlogFrontBodyFooterByWidgetItf $byWidget;

	public function __construct(
		BlogFrontBodyFooterCookieWidgetItf $cookieWidget,
		BlogFrontBodyFooterLicenseWidgetItf $licenseWidget,
		BlogFrontBodyFooterByWidgetItf $byWidget
	)
	{
		$this->cookieWidget = $cookieWidget;
		$this->licenseWidget = $licenseWidget;
		$this->byWidget = $byWidget;
	}

	public function render(BlogFrontBodyFooterWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(BlogFrontBodyFooterWidgetParamsItf $params):BlogFrontBodyFooterWidgetContextItf
		{
			return new BlogFrontBodyFooterWidgetContext(
				$this->renderCookieWidget(),
				$this->renderLicenseWidget(),
				$this->renderByWidget()
			);
		}

			private function renderCookieWidget():string
			{
				return $this->cookieWidget->render(
					$this->buildCookieWidgetParams()
				);
			}

				private function buildCookieWidgetParams():BlogFrontBodyFooterCookieWidgetParamsItf
				{
					return new BlogFrontBodyFooterCookieWidgetParams();
				}

			private function renderLicenseWidget():string
			{
				return $this->licenseWidget->render(
					$this->buildLicenseWidgetParams()
				);
			}

				private function buildLicenseWidgetParams():BlogFrontBodyFooterLicenseWidgetParamsItf
				{
					return new BlogFrontBodyFooterLicenseWidgetParams();
				}

			private function renderByWidget():string
			{
				return $this->byWidget->render(
					$this->buildByWidgetParams()
				);
			}

				private function buildByWidgetParams():BlogFrontBodyFooterByWidgetParamsItf
				{
					return new BlogFrontBodyFooterByWidgetParams();
				}
		
		private function renderByContext(BlogFrontBodyFooterWidgetContextItf $context):string
		{
			ob_start();
			require('blogFrontBodyFooterWidgetTemplate.php');
			return ob_get_clean();
		}

}