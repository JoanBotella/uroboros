<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main;

use blog\zone\front\view\widget\body\widget\main\BlogFrontBodyMainWidgetContextItf;

final class BlogFrontBodyMainWidgetContext implements BlogFrontBodyMainWidgetContextItf
{

	private string $header;
	private string $body;

	public function __construct(
		string $header,
		string $body
	)
	{
		$this->header = $header;
		$this->body = $body;
	}

	public function getHeader():string
	{
		return $this->header;
	}

	public function getBody():string
	{
		return $this->body;
	}

}