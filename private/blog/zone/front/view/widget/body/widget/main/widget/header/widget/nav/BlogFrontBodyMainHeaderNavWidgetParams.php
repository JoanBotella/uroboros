<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav;

use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetParamsItf;
use uroboros\localization\alternateTranslation\AlternateTranslationItf;

final class BlogFrontBodyMainHeaderNavWidgetParams implements BlogFrontBodyMainHeaderNavWidgetParamsItf
{

	/**
	 * @var array<AlternateTranslationItf> $alternateTranslations
	 */
	private array $alternateTranslations;

	/**
	 * @params array<AlternateTranslationItf> $alternateTranslations
	 */
	public function __construct(
		array $alternateTranslations
	)
	{
		$this->alternateTranslations = $alternateTranslations;
	}

	/**
	 * @return array<AlternateTranslationItf>
	 */
	public function getAlternateTranslations():array
	{
		return $this->alternateTranslations;
	}

}