<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body;

use blog\zone\front\view\widget\body\BlogFrontBodyWidgetScriptItf;

final class BlogFrontBodyWidgetScript implements BlogFrontBodyWidgetScriptItf
{

	private string $src;

	public function __construct(
		string $src
	)
	{
		$this->src = $src;
	}

	public function getSrc():string
	{
		return $this->src;
	}

}