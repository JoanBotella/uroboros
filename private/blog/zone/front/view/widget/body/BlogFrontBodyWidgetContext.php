<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body;

use blog\zone\front\view\widget\body\BlogFrontBodyWidgetContextItf;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetScriptItf;

final class BlogFrontBodyWidgetContext implements BlogFrontBodyWidgetContextItf
{

	private string $header;
	private string $main;
	private string $footer;
	/**
	 * @var array<BlogFrontBodyWidgetScriptItf> $scripts
	 */
	private array $scripts;

	/**
	 * @param array<BlogFrontBodyWidgetScriptItf> $scripts
	 */
	public function __construct(
		string $header,
		string $main,
		string $footer,
		array $scripts
	)
	{
		$this->header = $header;
		$this->main = $main;
		$this->footer = $footer;
		$this->scripts = $scripts;
	}

	public function getHeader():string
	{
		return $this->header;
	}

	public function getMain():string
	{
		return $this->main;
	}

	public function getFooter():string
	{
		return $this->footer;
	}

	/**
	 * @return array<BlogFrontBodyWidgetScriptItf>
	 */
	public function getScripts():array
	{
		return $this->scripts;
	}

}