<?php

use blog\zone\front\view\widget\body\BlogFrontBodyWidgetContextItf;

/**
 * @var BlogFrontBodyWidgetContextItf $context
 */

?><body class="blog-front-body-widget">

<?=$context->getHeader()?>

<div class="blog-front-body-widget__central_box">

<?php // <aside>hola</aside> ?>

<?=$context->getMain()?>

<?php // <aside>adios</aside> ?>

</div>

<?=$context->getFooter()?>

<?php foreach($context->getScripts() as $script) { ?>
	<script src="<?=$script->getSrc()?>"></script>
<?php } ?>
</body>
