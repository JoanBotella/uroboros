<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body;

interface BlogFrontBodyWidgetScriptItf
{

	public function getSrc():string;

}