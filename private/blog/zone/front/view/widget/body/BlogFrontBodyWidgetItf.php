<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body;

use blog\zone\front\view\widget\body\BlogFrontBodyWidgetParamsItf;

interface BlogFrontBodyWidgetItf
{

	public function render(BlogFrontBodyWidgetParamsItf $params):string;

}