<?php
declare(strict_types=1);

namespace blog\zone\front\view\widget\body\localization\translation;

use blog\zone\front\view\widget\body\widget\footer\localization\translation\BlogFrontBodyFooterWidgetTranslationCaTrait;

trait BlogFrontBodyWidgetTranslationCaTrait
{
	use
		BlogFrontBodyFooterWidgetTranslationCaTrait
	;
}