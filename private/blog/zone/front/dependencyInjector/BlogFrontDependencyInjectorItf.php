<?php
declare(strict_types=1);

namespace blog\zone\front\dependencyInjector;

use uroboros\zone\routeGenerator\RouteGeneratorItf;

use blog\zone\front\page\home\dependencyInjector\BlogFrontHomeDependencyInjectorItf;
use blog\zone\front\page\aboutMe\dependencyInjector\BlogFrontAboutMeDependencyInjectorItf;
use blog\zone\front\page\aboutBlog\dependencyInjector\BlogFrontAboutBlogDependencyInjectorItf;
use blog\zone\front\page\contact\dependencyInjector\BlogFrontContactDependencyInjectorItf;
use blog\zone\front\page\notFound\dependencyInjector\BlogFrontNotFoundDependencyInjectorItf;

interface
	BlogFrontDependencyInjectorItf
extends
	BlogFrontHomeDependencyInjectorItf,
	BlogFrontAboutMeDependencyInjectorItf,
	BlogFrontAboutBlogDependencyInjectorItf,
	BlogFrontNotFoundDependencyInjectorItf,
	BlogFrontContactDependencyInjectorItf
{

	public function getBlogFrontRouteGenerator():RouteGeneratorItf;

}