<?php
declare(strict_types=1);

namespace blog\zone\front\dependencyInjector;

use uroboros\zone\routeGenerator\RouteGeneratorItf;

use blog\zone\front\page\home\dependencyInjector\BlogFrontHomeDependencyInjectorTrait;
use blog\zone\front\page\aboutMe\dependencyInjector\BlogFrontAboutMeDependencyInjectorTrait;
use blog\zone\front\page\aboutBlog\dependencyInjector\BlogFrontAboutBlogDependencyInjectorTrait;
use blog\zone\front\page\contact\dependencyInjector\BlogFrontContactDependencyInjectorTrait;
use blog\zone\front\page\notFound\dependencyInjector\BlogFrontNotFoundDependencyInjectorTrait;
use blog\zone\front\view\widget\body\BlogFrontBodyWidget;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidget;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetItf;
use blog\zone\front\routeGenerator\BlogFrontRouteGenerator;
use blog\zone\front\view\widget\body\widget\footer\widget\by\BlogFrontBodyFooterByWidget;
use blog\zone\front\view\widget\body\widget\footer\widget\by\BlogFrontBodyFooterByWidgetItf;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\BlogFrontBodyFooterCookieWidget;
use blog\zone\front\view\widget\body\widget\footer\widget\cookie\BlogFrontBodyFooterCookieWidgetItf;
use blog\zone\front\view\widget\body\widget\footer\BlogFrontBodyFooterWidget;
use blog\zone\front\view\widget\body\widget\footer\BlogFrontBodyFooterWidgetItf;
use blog\zone\front\view\widget\body\widget\footer\widget\license\BlogFrontBodyFooterLicenseWidget;
use blog\zone\front\view\widget\body\widget\footer\widget\license\BlogFrontBodyFooterLicenseWidgetItf;
use blog\zone\front\view\widget\body\widget\header\BlogFrontBodyHeaderWidget;
use blog\zone\front\view\widget\body\widget\header\BlogFrontBodyHeaderWidgetItf;
use blog\zone\front\view\widget\body\widget\main\BlogFrontBodyMainWidget;
use blog\zone\front\view\widget\body\widget\main\BlogFrontBodyMainWidgetItf;
use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidget;
use blog\zone\front\view\widget\body\widget\main\widget\header\BlogFrontBodyMainHeaderWidgetItf;
use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidget;
use blog\zone\front\view\widget\body\widget\main\widget\header\widget\nav\BlogFrontBodyMainHeaderNavWidgetItf;

trait BlogFrontDependencyInjectorTrait
{
	use
		BlogFrontHomeDependencyInjectorTrait,
		BlogFrontAboutMeDependencyInjectorTrait,
		BlogFrontAboutBlogDependencyInjectorTrait,
		BlogFrontNotFoundDependencyInjectorTrait,
		BlogFrontContactDependencyInjectorTrait
	;

	public function getBlogFrontRouteGenerator():RouteGeneratorItf
	{
		return new BlogFrontRouteGenerator($this);
	}

	private function getBlogFrontBodyWidget():BlogFrontBodyWidgetItf
	{
		return new BlogFrontBodyWidget(
			$this->getBlogFrontBodyHeaderWidget(),
			$this->getBlogFrontBodyMainWidget(),
			$this->getBlogFrontBodyFooterWidget()
		);
	}

		private function getBlogFrontBodyHeaderWidget():BlogFrontBodyHeaderWidgetItf
		{
			return new BlogFrontBodyHeaderWidget(
				$this->getTranslationContainer(),
				$this->getBlogFrontHomeUrlBuilder(),
				$this->getBlogFrontBodyHeaderNavWidget()
			);
		}

			private function getBlogFrontBodyHeaderNavWidget():BlogFrontBodyHeaderNavWidgetItf
			{
				return new BlogFrontBodyHeaderNavWidget(
					$this->getTranslationContainer(),
					$this->getBlogFrontAboutMeUrlBuilder(),
					$this->getBlogFrontAboutBlogUrlBuilder(),
					$this->getBlogFrontContactUrlBuilder()
				);
			}

		private function getBlogFrontBodyMainWidget():BlogFrontBodyMainWidgetItf
		{
			return new BlogFrontBodyMainWidget(
				$this->getBlogFrontBodyMainHeaderWidget()
			);
		}

			private function getBlogFrontBodyMainHeaderWidget():BlogFrontBodyMainHeaderWidgetItf
			{
				return new BlogFrontBodyMainHeaderWidget(
					$this->getBlogFrontBodyMainHeaderNavWidget()
				);
			}

				private function getBlogFrontBodyMainHeaderNavWidget():BlogFrontBodyMainHeaderNavWidgetItf
				{
					return new BlogFrontBodyMainHeaderNavWidget();
				}

		private function getBlogFrontBodyFooterWidget():BlogFrontBodyFooterWidgetItf
		{
			return new BlogFrontBodyFooterWidget(
				$this->getBlogFrontBodyFooterCookieWidget(),
				$this->getBlogFrontBodyFooterLicenseWidget(),
				$this->getBlogFrontBodyFooterByWidget()
			);
		}

			private function getBlogFrontBodyFooterCookieWidget():BlogFrontBodyFooterCookieWidgetItf
			{
				return new BlogFrontBodyFooterCookieWidget(
					$this->getTranslationContainer()
				);
			}

			private function getBlogFrontBodyFooterLicenseWidget():BlogFrontBodyFooterLicenseWidgetItf
			{
				return new BlogFrontBodyFooterLicenseWidget(
					$this->getTranslationContainer()
				);
			}

			private function getBlogFrontBodyFooterByWidget():BlogFrontBodyFooterByWidgetItf
			{
				return new BlogFrontBodyFooterByWidget(
					$this->getTranslationContainer()
				);
			}

}