<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\route;

use blog\zone\front\page\home\dependencyInjector\BlogFrontHomeDependencyInjectorItf;
use uroboros\zone\page\route\RouteItf;
use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;

final class BlogFrontHomeRoute implements RouteItf
{

	private BlogFrontHomeDependencyInjectorItf $dependencyInjector;

	public function __construct(
		BlogFrontHomeDependencyInjectorItf $dependencyInjector
	)
	{
		$this->dependencyInjector = $dependencyInjector;
	}

	public function getMatcher():MatcherItf
	{
		return $this->dependencyInjector->getBlogFrontHomeMatcher();
	}

	public function getController():ControllerItf
	{
		return $this->dependencyInjector->getBlogFrontHomeController();
	}

}