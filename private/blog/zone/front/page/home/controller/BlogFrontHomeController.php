<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\controller;

use uroboros\zone\page\controller\ControllerItf;
use blog\zone\front\page\home\view\BlogFrontHomeViewItf;
use uroboros\request\RequestItf;
use uroboros\response\ResponseItf;

final class BlogFrontHomeController implements ControllerItf
{

	private BlogFrontHomeViewItf $view;

	public function __construct(
		BlogFrontHomeViewItf $view
	)
	{
		$this->view = $view;
	}

	public function action(RequestItf $request):ResponseItf
	{
		return $this->view->render();
	}

}