<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\matcher;

use blog\matcher\BlogMatcherAbs;
use uroboros\zone\page\matcher\MatcherItf;
use uroboros\request\RequestItf;

final class BlogFrontHomeMatcher extends BlogMatcherAbs implements MatcherItf
{

	public function match(RequestItf $request):bool
	{
		$requestPath = $request->getPath();

		if ($requestPath == '')
		{
			return true;
		}

		$translation = $this->getTranslation();
		$languageCode = $translation->getLanguageCode();

		$requestPathSegments = $request->getPathSegments();

		if (
			count($requestPathSegments) == 1
			&& $requestPathSegments[0] == $languageCode
		)
		{
			return true;
		}

		$pagePath = $languageCode.'/'.$translation->blogFrontHome_slug();

		return $requestPath == $pagePath;
	}

}