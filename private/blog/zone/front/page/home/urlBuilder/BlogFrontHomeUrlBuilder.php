<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\urlBuilder;

use blog\zone\front\page\home\urlBuilder\BlogFrontHomeUrlBuilderItf;
use blog\urlBuilder\BlogUrlBuilderAbs;

final class BlogFrontHomeUrlBuilder extends BlogUrlBuilderAbs implements BlogFrontHomeUrlBuilderItf
{

	public function buildRelative(string $languageCode):string
	{
		return $languageCode.'/'.$this->getTranslation($languageCode)->blogFrontHome_slug();
	}

	public function buildAbsolute(string $languageCode):string
	{
		return $this->getBaseUrl().$this->buildRelative($languageCode);
	}

}