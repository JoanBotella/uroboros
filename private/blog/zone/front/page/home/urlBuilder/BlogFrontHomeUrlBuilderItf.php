<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\urlBuilder;

interface BlogFrontHomeUrlBuilderItf
{

	public function buildRelative(string $languageCode):string;

	public function buildAbsolute(string $languageCode):string;

}