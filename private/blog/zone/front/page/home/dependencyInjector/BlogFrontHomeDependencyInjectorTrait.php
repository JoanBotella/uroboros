<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\dependencyInjector;

use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;
use uroboros\view\html\widget\html\HtmlWidgetItf;

use blog\zone\front\page\home\matcher\BlogFrontHomeMatcher;
use blog\zone\front\page\home\controller\BlogFrontHomeController;
use blog\zone\front\page\home\urlBuilder\BlogFrontHomeUrlBuilder;
use blog\zone\front\page\home\urlBuilder\BlogFrontHomeUrlBuilderItf;
use blog\zone\front\page\home\view\BlogFrontHomeView;
use blog\zone\front\page\home\view\BlogFrontHomeViewItf;
use blog\zone\front\page\home\view\widget\mainBody\BlogFrontHomeMainBodyWidget;
use blog\zone\front\page\home\view\widget\mainBody\BlogFrontHomeMainBodyWidgetItf;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use uroboros\configurationContainer\ConfigurationContainerItf;

trait BlogFrontHomeDependencyInjectorTrait
{

	public function getBlogFrontHomeMatcher():MatcherItf
	{
		return new BlogFrontHomeMatcher(
			$this->getTranslationContainer()
		);
	}

	public function getBlogFrontHomeController():ControllerItf
	{
		return new BlogFrontHomeController(
			$this->getBlogFrontHomeView()
		);
	}

		private function getBlogFrontHomeView():BlogFrontHomeViewItf
		{
			return new BlogFrontHomeView(
				$this->getHtmlWidget(),
				$this->getTranslationContainer(),
				$this->getBlogFrontBodyWidget(),
				$this->getBlogFrontHomeUrlBuilder(),
				$this->getBlogFrontHomeMainBodyWidget()
			);
		}

			abstract private function getHtmlWidget():HtmlWidgetItf;

			abstract private function getTranslationContainer():BlogTranslationContainerItf;

			abstract private function getBlogFrontBodyWidget():BlogFrontBodyWidgetItf;

			abstract private function getBlogFrontBodyHeaderNavWidget():BlogFrontBodyHeaderNavWidgetItf;

			private function getBlogFrontHomeMainBodyWidget():BlogFrontHomeMainBodyWidgetItf
			{
				return new BlogFrontHomeMainBodyWidget(
					$this->getTranslationContainer()
				);
			}

		private function getBlogFrontHomeUrlBuilder():BlogFrontHomeUrlBuilderItf
		{
			return new BlogFrontHomeUrlBuilder(
				$this->getConfigurationContainer(),
				$this->getTranslationContainer()
			);
		}

			abstract private function getConfigurationContainer():ConfigurationContainerItf;

}
