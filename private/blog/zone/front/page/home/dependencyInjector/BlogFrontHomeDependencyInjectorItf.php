<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\dependencyInjector;

use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;

interface BlogFrontHomeDependencyInjectorItf
{

	public function getBlogFrontHomeMatcher():MatcherItf;

	public function getBlogFrontHomeController():ControllerItf;

}