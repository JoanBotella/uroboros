<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\localization\translation;

use blog\zone\front\page\home\view\widget\mainBody\localization\translation\BlogFrontHomeMainBodyWidgetTranslationItf;

interface BlogFrontHomeTranslationItf
extends
	BlogFrontHomeMainBodyWidgetTranslationItf
{

	public function blogFrontHome_slug():string;

	public function blogFrontHome_pageTitle():string;

}