<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\localization\translation;

use blog\zone\front\page\home\view\widget\mainBody\localization\translation\BlogFrontHomeMainBodyWidgetTranslationEnTrait;

trait BlogFrontHomeTranslationEnTrait
{
	use
		BlogFrontHomeMainBodyWidgetTranslationEnTrait
	;

	public function blogFrontHome_slug():string { return 'home'; }

	public function blogFrontHome_pageTitle():string { return 'Home'; }

}