<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\view;

use uroboros\response\ResponseItf;
use uroboros\view\html\widget\html\HtmlWidgetItf;

use blog\localization\translation\BlogTranslationItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use blog\zone\front\page\home\urlBuilder\BlogFrontHomeUrlBuilderItf;
use blog\zone\front\page\home\view\BlogFrontHomeViewItf;
use blog\zone\front\page\home\view\widget\mainBody\BlogFrontHomeMainBodyWidgetItf;
use blog\zone\front\page\home\view\widget\mainBody\BlogFrontHomeMainBodyWidgetParams;
use blog\zone\front\page\home\view\widget\mainBody\BlogFrontHomeMainBodyWidgetParamsItf;
use blog\zone\front\view\BlogFrontViewAbs;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;

final class BlogFrontHomeView extends BlogFrontViewAbs implements BlogFrontHomeViewItf
{

	private BlogFrontHomeUrlBuilderItf $urlBuilder;
	private BlogFrontHomeMainBodyWidgetItf $mainBodyWidget;

	public function __construct(
		HtmlWidgetItf $htmlWidget,
		BlogTranslationContainerItf $translationContainer,
		BlogFrontBodyWidgetItf $bodyWidget,
		BlogFrontHomeUrlBuilderItf $urlBuilder,
		BlogFrontHomeMainBodyWidgetItf $mainBodyWidget
	)
	{
		parent::__construct(
			$htmlWidget,
			$translationContainer,
			$bodyWidget
		);
		$this->urlBuilder = $urlBuilder;
		$this->mainBodyWidget = $mainBodyWidget;
	}

	public function render():ResponseItf
	{
		return $this->buildResponse();
	}

	protected function renderMainBodyWidget():string
	{
		return $this->mainBodyWidget->render(
			$this->buildMainBodyWidgetParams()
		);
	}

		private function buildMainBodyWidgetParams():BlogFrontHomeMainBodyWidgetParamsItf
		{
			return new BlogFrontHomeMainBodyWidgetParams(
				$this->getPageTitle()
			);
		}

	protected function getPageTitleByTranslation(BlogTranslationItf $translation):string
	{
		return $translation->blogFrontHome_pageTitle();
	}

	protected function buildAbsoluteUrlByLanguageCode(string $languageCode):string
	{
		return $this->urlBuilder->buildAbsolute($languageCode);
	}

}