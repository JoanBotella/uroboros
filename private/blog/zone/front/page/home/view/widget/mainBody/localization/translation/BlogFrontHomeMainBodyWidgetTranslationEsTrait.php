<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\view\widget\mainBody\localization\translation;

trait BlogFrontHomeMainBodyWidgetTranslationEsTrait
{

	public function blogFrontHomeMainBody_content():string
	{
		return <<<END
<figure><img src="asset/blog/zone/front/page/home/landing.gif" /></figure>
<p>¡Enhorabuena! Acabas de aterrizar en mi página personal, mi pequeña hacienda en el ciberespacio, en forma de blog. Sí, ya sé que es el año nosecuantos y los blogs están pasados de moda, pero, por lo que sea, últimamente todo a mi alrededor me recuerda que estoy viejo y que debería añorar los 70, 80 y 90. Los 2000 ya me pillaron mayor, y los recuerdo como una mierda de década. Bueno, pues eso. Un blog. Por coherencia.</p>
END;
	}

}