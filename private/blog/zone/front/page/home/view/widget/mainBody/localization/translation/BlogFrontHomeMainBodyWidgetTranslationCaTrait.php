<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\view\widget\mainBody\localization\translation;

trait BlogFrontHomeMainBodyWidgetTranslationCaTrait
{

	public function blogFrontHomeMainBody_content():string
	{
		return <<<END
<p>Esta pàgina està solament en castellà!</p>
END;
	}

}