<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\view\widget\mainBody\localization\translation;

interface BlogFrontHomeMainBodyWidgetTranslationItf
{

	public function blogFrontHomeMainBody_content():string;

}