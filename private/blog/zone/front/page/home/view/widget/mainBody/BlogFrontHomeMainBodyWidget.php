<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\view\widget\mainBody;

use blog\zone\front\page\home\view\widget\mainBody\BlogFrontHomeMainBodyWidgetItf;
use blog\zone\front\page\home\view\widget\mainBody\BlogFrontHomeMainBodyWidgetParamsItf;
use blog\zone\front\page\home\view\widget\mainBody\BlogFrontHomeMainBodyWidgetContext;
use blog\zone\front\page\home\view\widget\mainBody\BlogFrontHomeMainBodyWidgetContextItf;
use blog\widget\BlogTranslatableWidgetAbs;

final class BlogFrontHomeMainBodyWidget extends BlogTranslatableWidgetAbs implements BlogFrontHomeMainBodyWidgetItf
{

	public function render(BlogFrontHomeMainBodyWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(BlogFrontHomeMainBodyWidgetParamsItf $params):BlogFrontHomeMainBodyWidgetContextItf
		{
			return new BlogFrontHomeMainBodyWidgetContext();
		}

		private function renderByContext(BlogFrontHomeMainBodyWidgetContextItf $context):string
		{
			$translation = $this->getTranslation();
			ob_start();
			require('blogFrontHomeMainBodyWidgetTemplate.php');
			return ob_get_clean();
		}

}