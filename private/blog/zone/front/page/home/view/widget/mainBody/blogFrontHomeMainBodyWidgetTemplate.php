<?php

use blog\zone\front\page\home\view\widget\mainBody\BlogFrontHomeMainBodyWidgetContextItf;
use blog\zone\front\page\home\view\widget\mainBody\localization\translation\BlogFrontHomeMainBodyWidgetTranslationItf;

/**
 * @var BlogFrontHomeMainBodyWidgetContextItf $context
 * @var BlogFrontHomeMainBodyWidgetTranslationItf $translation
 */

?>	<div class="blog-front-home-main_body-widget">
<?=$translation->blogFrontHomeMainBody_content();?>
	</div>
