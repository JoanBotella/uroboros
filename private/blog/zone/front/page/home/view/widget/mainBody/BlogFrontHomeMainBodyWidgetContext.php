<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\view\widget\mainBody;

use blog\zone\front\page\home\view\widget\mainBody\BlogFrontHomeMainBodyWidgetContextItf;

final class BlogFrontHomeMainBodyWidgetContext implements BlogFrontHomeMainBodyWidgetContextItf
{
}