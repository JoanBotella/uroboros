<?php
declare(strict_types=1);

namespace blog\zone\front\page\home\view;

use uroboros\response\ResponseItf;

interface BlogFrontHomeViewItf
{

	public function render():ResponseItf;

}