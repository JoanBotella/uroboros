<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\urlBuilder;

use blog\zone\front\page\contact\urlBuilder\BlogFrontContactUrlBuilderItf;
use blog\urlBuilder\BlogUrlBuilderAbs;

final class BlogFrontContactUrlBuilder extends BlogUrlBuilderAbs implements BlogFrontContactUrlBuilderItf
{

	public function buildRelative(string $languageCode):string
	{
		return $languageCode.'/'.$this->getTranslation($languageCode)->blogFrontContact_slug();
	}

	public function buildAbsolute(string $languageCode):string
	{
		return $this->getBaseUrl().$this->buildRelative($languageCode);
	}

}