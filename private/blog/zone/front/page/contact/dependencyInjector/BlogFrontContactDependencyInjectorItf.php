<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\dependencyInjector;

use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;

interface BlogFrontContactDependencyInjectorItf
{

	public function getBlogFrontContactMatcher():MatcherItf;

	public function getBlogFrontContactController():ControllerItf;

}