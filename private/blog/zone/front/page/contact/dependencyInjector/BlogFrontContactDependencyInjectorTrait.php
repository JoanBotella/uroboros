<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\dependencyInjector;

use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;
use uroboros\view\html\widget\html\HtmlWidgetItf;

use blog\zone\front\page\contact\matcher\BlogFrontContactMatcher;
use blog\zone\front\page\contact\controller\BlogFrontContactController;
use blog\zone\front\page\contact\urlBuilder\BlogFrontContactUrlBuilder;
use blog\zone\front\page\contact\urlBuilder\BlogFrontContactUrlBuilderItf;
use blog\zone\front\page\contact\view\BlogFrontContactView;
use blog\zone\front\page\contact\view\BlogFrontContactViewItf;
use blog\zone\front\page\contact\view\widget\mainBody\BlogFrontContactMainBodyWidget;
use blog\zone\front\page\contact\view\widget\mainBody\BlogFrontContactMainBodyWidgetItf;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use uroboros\configurationContainer\ConfigurationContainerItf;

trait BlogFrontContactDependencyInjectorTrait
{

	public function getBlogFrontContactMatcher():MatcherItf
	{
		return new BlogFrontContactMatcher(
			$this->getTranslationContainer()
		);
	}

	public function getBlogFrontContactController():ControllerItf
	{
		return new BlogFrontContactController(
			$this->getBlogFrontContactView()
		);
	}

		private function getBlogFrontContactView():BlogFrontContactViewItf
		{
			return new BlogFrontContactView(
				$this->getHtmlWidget(),
				$this->getTranslationContainer(),
				$this->getBlogFrontBodyWidget(),
				$this->getBlogFrontContactUrlBuilder(),
				$this->getBlogFrontContactMainBodyWidget()
			);
		}

			abstract private function getHtmlWidget():HtmlWidgetItf;

			abstract private function getTranslationContainer():BlogTranslationContainerItf;

			abstract private function getBlogFrontBodyWidget():BlogFrontBodyWidgetItf;

			abstract private function getBlogFrontBodyHeaderNavWidget():BlogFrontBodyHeaderNavWidgetItf;

			private function getBlogFrontContactMainBodyWidget():BlogFrontContactMainBodyWidgetItf
			{
				return new BlogFrontContactMainBodyWidget(
					$this->getTranslationContainer()
				);
			}

		private function getBlogFrontContactUrlBuilder():BlogFrontContactUrlBuilderItf
		{
			return new BlogFrontContactUrlBuilder(
				$this->getConfigurationContainer(),
				$this->getTranslationContainer()
			);
		}

			abstract private function getConfigurationContainer():ConfigurationContainerItf;

}
