<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\matcher;

use blog\matcher\BlogMatcherAbs;
use uroboros\zone\page\matcher\MatcherItf;
use uroboros\request\RequestItf;

final class BlogFrontContactMatcher extends BlogMatcherAbs implements MatcherItf
{

	public function match(RequestItf $request):bool
	{
		$translation = $this->getTranslation();
		$pagePath = $translation->getLanguageCode().'/'.$translation->blogFrontContact_slug();
		return $request->getPath() == $pagePath;
	}

}