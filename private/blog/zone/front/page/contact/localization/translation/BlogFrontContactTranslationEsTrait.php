<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\localization\translation;

use blog\zone\front\page\contact\view\widget\mainBody\localization\translation\BlogFrontContactMainBodyWidgetTranslationEsTrait;

trait BlogFrontContactTranslationEsTrait
{
	use
		BlogFrontContactMainBodyWidgetTranslationEsTrait
	;

	public function blogFrontContact_slug():string { return 'contacto'; }

	public function blogFrontContact_pageTitle():string { return 'Contacto'; }

}