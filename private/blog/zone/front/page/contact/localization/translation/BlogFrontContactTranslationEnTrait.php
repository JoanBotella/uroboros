<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\localization\translation;

use blog\zone\front\page\contact\view\widget\mainBody\localization\translation\BlogFrontContactMainBodyWidgetTranslationEnTrait;

trait BlogFrontContactTranslationEnTrait
{
	use
		BlogFrontContactMainBodyWidgetTranslationEnTrait
	;

	public function blogFrontContact_slug():string { return 'contact'; }

	public function blogFrontContact_pageTitle():string { return 'Contact'; }

}