<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\localization\translation;

use blog\zone\front\page\contact\view\widget\mainBody\localization\translation\BlogFrontContactMainBodyWidgetTranslationItf;

interface BlogFrontContactTranslationItf
extends
	BlogFrontContactMainBodyWidgetTranslationItf
{

	public function blogFrontContact_slug():string;

	public function blogFrontContact_pageTitle():string;

}