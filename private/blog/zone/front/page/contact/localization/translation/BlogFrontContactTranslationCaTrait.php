<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\localization\translation;

use blog\zone\front\page\contact\view\widget\mainBody\localization\translation\BlogFrontContactMainBodyWidgetTranslationCaTrait;

trait BlogFrontContactTranslationCaTrait
{
	use
		BlogFrontContactMainBodyWidgetTranslationCaTrait
	;

	public function blogFrontContact_slug():string { return 'contacte'; }

	public function blogFrontContact_pageTitle():string { return 'Contacte'; }

}