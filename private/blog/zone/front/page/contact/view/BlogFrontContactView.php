<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\view;

use uroboros\response\ResponseItf;
use uroboros\view\html\widget\html\HtmlWidgetItf;

use blog\localization\translation\BlogTranslationItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use blog\zone\front\page\contact\urlBuilder\BlogFrontContactUrlBuilderItf;
use blog\zone\front\page\contact\view\BlogFrontContactViewItf;
use blog\zone\front\page\contact\view\widget\mainBody\BlogFrontContactMainBodyWidgetItf;
use blog\zone\front\page\contact\view\widget\mainBody\BlogFrontContactMainBodyWidgetParams;
use blog\zone\front\page\contact\view\widget\mainBody\BlogFrontContactMainBodyWidgetParamsItf;
use blog\zone\front\view\BlogFrontViewAbs;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetScript;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetScriptItf;

final class BlogFrontContactView extends BlogFrontViewAbs implements BlogFrontContactViewItf
{

	private BlogFrontContactUrlBuilderItf $urlBuilder;
	private BlogFrontContactMainBodyWidgetItf $mainBodyWidget;

	public function __construct(
		HtmlWidgetItf $htmlWidget,
		BlogTranslationContainerItf $translationContainer,
		BlogFrontBodyWidgetItf $bodyWidget,
		BlogFrontContactUrlBuilderItf $urlBuilder,
		BlogFrontContactMainBodyWidgetItf $mainBodyWidget
	)
	{
		parent::__construct(
			$htmlWidget,
			$translationContainer,
			$bodyWidget
		);
		$this->urlBuilder = $urlBuilder;
		$this->mainBodyWidget = $mainBodyWidget;
	}

	public function render():ResponseItf
	{
		return $this->buildResponse();
	}

	protected function renderMainBodyWidget():string
	{
		return $this->mainBodyWidget->render(
			$this->buildMainBodyWidgetParams()
		);
	}

		private function buildMainBodyWidgetParams():BlogFrontContactMainBodyWidgetParamsItf
		{
			return new BlogFrontContactMainBodyWidgetParams(
				$this->getPageTitle()
			);
		}

	protected function getPageTitleByTranslation(BlogTranslationItf $translation):string
	{
		return $translation->blogFrontContact_pageTitle();
	}

	protected function buildAbsoluteUrlByLanguageCode(string $languageCode):string
	{
		return $this->urlBuilder->buildAbsolute($languageCode);
	}

	/**
	 * @return array<BlogFrontBodyWidgetScriptItf>
	 */
	protected function getScripts():array
	{
		$scripts = parent::getScripts();
		$scripts[] = new BlogFrontBodyWidgetScript('asset/blog/zone/front/page/contact/blogFrontContact.js');
		return $scripts;
	}

}