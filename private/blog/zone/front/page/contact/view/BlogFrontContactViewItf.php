<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\view;

use uroboros\response\ResponseItf;

interface BlogFrontContactViewItf
{

	public function render():ResponseItf;

}