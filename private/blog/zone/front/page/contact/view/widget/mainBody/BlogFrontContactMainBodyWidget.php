<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\view\widget\mainBody;

use blog\zone\front\page\contact\view\widget\mainBody\BlogFrontContactMainBodyWidgetItf;
use blog\zone\front\page\contact\view\widget\mainBody\BlogFrontContactMainBodyWidgetParamsItf;
use blog\zone\front\page\contact\view\widget\mainBody\BlogFrontContactMainBodyWidgetContext;
use blog\zone\front\page\contact\view\widget\mainBody\BlogFrontContactMainBodyWidgetContextItf;
use blog\widget\BlogTranslatableWidgetAbs;

final class BlogFrontContactMainBodyWidget extends BlogTranslatableWidgetAbs implements BlogFrontContactMainBodyWidgetItf
{

	public function render(BlogFrontContactMainBodyWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(BlogFrontContactMainBodyWidgetParamsItf $params):BlogFrontContactMainBodyWidgetContextItf
		{
			return new BlogFrontContactMainBodyWidgetContext();
		}

		private function renderByContext(BlogFrontContactMainBodyWidgetContextItf $context):string
		{
			$translation = $this->getTranslation();
			ob_start();
			require('blogFrontContactMainBodyWidgetTemplate.php');
			return ob_get_clean();
		}

}