<?php

use blog\zone\front\page\contact\view\widget\mainBody\BlogFrontContactMainBodyWidgetContextItf;
use blog\zone\front\page\contact\view\widget\mainBody\localization\translation\BlogFrontContactMainBodyWidgetTranslationItf;

/**
 * @var BlogFrontContactMainBodyWidgetContextItf $context
 * @var BlogFrontContactMainBodyWidgetTranslationItf $translation
 */

?>	<div class="blog-front-contact-main_body-widget">
<?=$translation->blogFrontContactMainBody_content();?>
	</div>
