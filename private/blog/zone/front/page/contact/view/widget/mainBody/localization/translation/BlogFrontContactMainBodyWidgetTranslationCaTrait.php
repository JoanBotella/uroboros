<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\view\widget\mainBody\localization\translation;

trait BlogFrontContactMainBodyWidgetTranslationCaTrait
{

	public function blogFrontContactMainBody_content():string
	{
		return <<<END
<p>Esta pàgina està solament en castellà!</p>
END;
	}

}