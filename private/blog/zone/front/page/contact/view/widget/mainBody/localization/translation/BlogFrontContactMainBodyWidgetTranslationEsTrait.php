<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\view\widget\mainBody\localization\translation;

trait BlogFrontContactMainBodyWidgetTranslationEsTrait
{
	public function blogFrontContactMainBody_content():string
	{
		return <<<END
<p>Algún día, en esta página habrá un flamante formulario de contacto, que servirá para que puedas enviarme mensajes sin que tenga que exponer mi dirección de e-mail a todo Internet.</p>
<p>Mientras tanto, he habilitado una cuenta de correo sólo para el blog, por si me la quieres llenar de SPAM y tal. Podría poner aquí la dirección directamente, en plano, pero algún bot la leería y me enviaría anuncios de Viagra. También podría escribir el texto en una imagen para ponérselo más difícil, pero me consta que siguen pillándola. En lugar de eso, te explico cómo adivinarla:</p>
<ul>
	<li>Empieza por "blog".</li>
	<li>Después viene el símbolo ése que tienen todas las direcciones de e-mail.</li>
	<li>A continuación pones mi nombre y primer apellido, todo junto, sin espacios y en minúsculas.</li>
	<li>Y, finalmente, añades el punto com.</li>
</ul>
<p>Si has seguido correctamente los pasos anteriores, ahora tendrás una dirección de e-mail. Puedes enviar ahí tus felicitaciones por mi gusto estético en cuestiones de páginas web personales, o lo que quieras. Si no recibes un correo indicándote que la cuenta no existe, probablemente habrás escrito bien la dirección. Suelo mirar la bandeja de entrada todos los días, así que no creo que tarde más de 2 o 3 en contestarte, pero recuerda que shift happens.</p>
<p>Perdona las molestias.</p>
<figure>
	<a data-hook="blog-front-contact-main_body__email_anchor" href="mailto:fingolfin69@gmail.com" title="Enviar un e-mail a esta dirección.">
		<img src="asset/blog/zone/front/page/contact/email.png" alt="fingolfin69@gmail.com" />
	</a>
	<figcaption>Esta <strong>no</strong> es mi dirección de e-mail.</figcaption>
</figure>
END;
	}

}