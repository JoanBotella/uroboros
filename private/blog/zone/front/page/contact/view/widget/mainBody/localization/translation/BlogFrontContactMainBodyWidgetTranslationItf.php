<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\view\widget\mainBody\localization\translation;

interface BlogFrontContactMainBodyWidgetTranslationItf
{

	public function blogFrontContactMainBody_content():string;

}