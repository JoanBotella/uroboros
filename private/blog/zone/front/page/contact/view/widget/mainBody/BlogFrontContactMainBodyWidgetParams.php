<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\view\widget\mainBody;

use blog\zone\front\page\contact\view\widget\mainBody\BlogFrontContactMainBodyWidgetParamsItf;

final class BlogFrontContactMainBodyWidgetParams implements BlogFrontContactMainBodyWidgetParamsItf
{
}