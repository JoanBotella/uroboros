<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\controller;

use uroboros\zone\page\controller\ControllerItf;
use blog\zone\front\page\contact\view\BlogFrontContactViewItf;
use uroboros\request\RequestItf;
use uroboros\response\ResponseItf;

final class BlogFrontContactController implements ControllerItf
{

	private BlogFrontContactViewItf $view;

	public function __construct(
		BlogFrontContactViewItf $view
	)
	{
		$this->view = $view;
	}

	public function action(RequestItf $request):ResponseItf
	{
		return $this->view->render();
	}

}