<?php
declare(strict_types=1);

namespace blog\zone\front\page\contact\route;

use blog\zone\front\page\contact\dependencyInjector\BlogFrontContactDependencyInjectorItf;
use uroboros\zone\page\route\RouteItf;
use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;

final class BlogFrontContactRoute implements RouteItf
{

	private BlogFrontContactDependencyInjectorItf $dependencyInjector;

	public function __construct(
		BlogFrontContactDependencyInjectorItf $dependencyInjector
	)
	{
		$this->dependencyInjector = $dependencyInjector;
	}

	public function getMatcher():MatcherItf
	{
		return $this->dependencyInjector->getBlogFrontContactMatcher();
	}

	public function getController():ControllerItf
	{
		return $this->dependencyInjector->getBlogFrontContactController();
	}

}