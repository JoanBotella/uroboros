<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\matcher;

use blog\matcher\BlogMatcherAbs;
use uroboros\zone\page\matcher\MatcherItf;
use uroboros\request\RequestItf;

final class BlogFrontAboutMeMatcher extends BlogMatcherAbs implements MatcherItf
{

	public function match(RequestItf $request):bool
	{
		$translation = $this->getTranslation();
		$pagePath = $translation->getLanguageCode().'/'.$translation->blogFrontAboutMe_slug();
		return $request->getPath() == $pagePath;
	}

}