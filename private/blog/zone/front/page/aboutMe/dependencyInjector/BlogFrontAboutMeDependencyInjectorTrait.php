<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\dependencyInjector;

use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;
use uroboros\view\html\widget\html\HtmlWidgetItf;

use blog\zone\front\page\aboutMe\matcher\BlogFrontAboutMeMatcher;
use blog\zone\front\page\aboutMe\controller\BlogFrontAboutMeController;
use blog\zone\front\page\aboutMe\urlBuilder\BlogFrontAboutMeUrlBuilder;
use blog\zone\front\page\aboutMe\urlBuilder\BlogFrontAboutMeUrlBuilderItf;
use blog\zone\front\page\aboutMe\view\BlogFrontAboutMeView;
use blog\zone\front\page\aboutMe\view\BlogFrontAboutMeViewItf;
use blog\zone\front\page\aboutMe\view\widget\mainBody\BlogFrontAboutMeMainBodyWidget;
use blog\zone\front\page\aboutMe\view\widget\mainBody\BlogFrontAboutMeMainBodyWidgetItf;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use uroboros\configurationContainer\ConfigurationContainerItf;

trait BlogFrontAboutMeDependencyInjectorTrait
{

	public function getBlogFrontAboutMeMatcher():MatcherItf
	{
		return new BlogFrontAboutMeMatcher(
			$this->getTranslationContainer()
		);
	}

	public function getBlogFrontAboutMeController():ControllerItf
	{
		return new BlogFrontAboutMeController(
			$this->getBlogFrontAboutMeView()
		);
	}

		private function getBlogFrontAboutMeView():BlogFrontAboutMeViewItf
		{
			return new BlogFrontAboutMeView(
				$this->getHtmlWidget(),
				$this->getTranslationContainer(),
				$this->getBlogFrontBodyWidget(),
				$this->getBlogFrontAboutMeUrlBuilder(),
				$this->getBlogFrontAboutMeMainBodyWidget()
			);
		}

			abstract private function getHtmlWidget():HtmlWidgetItf;

			abstract private function getTranslationContainer():BlogTranslationContainerItf;

			abstract private function getBlogFrontBodyWidget():BlogFrontBodyWidgetItf;

			abstract private function getBlogFrontBodyHeaderNavWidget():BlogFrontBodyHeaderNavWidgetItf;

			private function getBlogFrontAboutMeMainBodyWidget():BlogFrontAboutMeMainBodyWidgetItf
			{
				return new BlogFrontAboutMeMainBodyWidget(
					$this->getTranslationContainer()
				);
			}

		private function getBlogFrontAboutMeUrlBuilder():BlogFrontAboutMeUrlBuilderItf
		{
			return new BlogFrontAboutMeUrlBuilder(
				$this->getConfigurationContainer(),
				$this->getTranslationContainer()
			);
		}

			abstract private function getConfigurationContainer():ConfigurationContainerItf;

}
