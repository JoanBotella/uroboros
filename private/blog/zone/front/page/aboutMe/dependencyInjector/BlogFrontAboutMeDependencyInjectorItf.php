<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\dependencyInjector;

use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;

interface BlogFrontAboutMeDependencyInjectorItf
{

	public function getBlogFrontAboutMeMatcher():MatcherItf;

	public function getBlogFrontAboutMeController():ControllerItf;

}