<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\controller;

use uroboros\zone\page\controller\ControllerItf;
use blog\zone\front\page\aboutMe\view\BlogFrontAboutMeViewItf;
use uroboros\request\RequestItf;
use uroboros\response\ResponseItf;

final class BlogFrontAboutMeController implements ControllerItf
{

	private BlogFrontAboutMeViewItf $view;

	public function __construct(
		BlogFrontAboutMeViewItf $view
	)
	{
		$this->view = $view;
	}

	public function action(RequestItf $request):ResponseItf
	{
		return $this->view->render();
	}

}