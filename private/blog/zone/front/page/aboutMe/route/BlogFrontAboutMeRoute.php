<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\route;

use blog\zone\front\page\aboutMe\dependencyInjector\BlogFrontAboutMeDependencyInjectorItf;
use uroboros\zone\page\route\RouteItf;
use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;

final class BlogFrontAboutMeRoute implements RouteItf
{

	private BlogFrontAboutMeDependencyInjectorItf $dependencyInjector;

	public function __construct(
		BlogFrontAboutMeDependencyInjectorItf $dependencyInjector
	)
	{
		$this->dependencyInjector = $dependencyInjector;
	}

	public function getMatcher():MatcherItf
	{
		return $this->dependencyInjector->getBlogFrontAboutMeMatcher();
	}

	public function getController():ControllerItf
	{
		return $this->dependencyInjector->getBlogFrontAboutMeController();
	}

}