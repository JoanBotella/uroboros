<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\localization\translation;

use blog\zone\front\page\aboutMe\view\widget\mainBody\localization\translation\BlogFrontAboutMeMainBodyWidgetTranslationEsTrait;

trait BlogFrontAboutMeTranslationEsTrait
{
	use
		BlogFrontAboutMeMainBodyWidgetTranslationEsTrait
	;

	public function blogFrontAboutMe_slug():string { return 'sobre-mi'; }

	public function blogFrontAboutMe_pageTitle():string { return 'Sobre Mí'; }

}