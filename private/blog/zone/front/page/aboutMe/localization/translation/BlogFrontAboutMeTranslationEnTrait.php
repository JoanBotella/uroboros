<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\localization\translation;

use blog\zone\front\page\aboutMe\view\widget\mainBody\localization\translation\BlogFrontAboutMeMainBodyWidgetTranslationEnTrait;

trait BlogFrontAboutMeTranslationEnTrait
{
	use
		BlogFrontAboutMeMainBodyWidgetTranslationEnTrait
	;

	public function blogFrontAboutMe_slug():string { return 'about-me'; }

	public function blogFrontAboutMe_pageTitle():string { return 'About Me'; }

}