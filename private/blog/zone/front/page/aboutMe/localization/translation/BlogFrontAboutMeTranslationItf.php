<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\localization\translation;

use blog\zone\front\page\aboutMe\view\widget\mainBody\localization\translation\BlogFrontAboutMeMainBodyWidgetTranslationItf;

interface BlogFrontAboutMeTranslationItf
extends
	BlogFrontAboutMeMainBodyWidgetTranslationItf
{

	public function blogFrontAboutMe_slug():string;

	public function blogFrontAboutMe_pageTitle():string;

}