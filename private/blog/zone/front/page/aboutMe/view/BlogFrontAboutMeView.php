<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\view;

use uroboros\response\ResponseItf;
use uroboros\view\html\widget\html\HtmlWidgetItf;

use blog\localization\translation\BlogTranslationItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use blog\zone\front\page\aboutMe\urlBuilder\BlogFrontAboutMeUrlBuilderItf;
use blog\zone\front\page\aboutMe\view\BlogFrontAboutMeViewItf;
use blog\zone\front\page\aboutMe\view\widget\mainBody\BlogFrontAboutMeMainBodyWidgetItf;
use blog\zone\front\page\aboutMe\view\widget\mainBody\BlogFrontAboutMeMainBodyWidgetParams;
use blog\zone\front\page\aboutMe\view\widget\mainBody\BlogFrontAboutMeMainBodyWidgetParamsItf;
use blog\zone\front\view\BlogFrontViewAbs;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;

final class BlogFrontAboutMeView extends BlogFrontViewAbs implements BlogFrontAboutMeViewItf
{

	private BlogFrontAboutMeUrlBuilderItf $urlBuilder;
	private BlogFrontAboutMeMainBodyWidgetItf $mainBodyWidget;

	public function __construct(
		HtmlWidgetItf $htmlWidget,
		BlogTranslationContainerItf $translationContainer,
		BlogFrontBodyWidgetItf $bodyWidget,
		BlogFrontAboutMeUrlBuilderItf $urlBuilder,
		BlogFrontAboutMeMainBodyWidgetItf $mainBodyWidget
	)
	{
		parent::__construct(
			$htmlWidget,
			$translationContainer,
			$bodyWidget
		);
		$this->urlBuilder = $urlBuilder;
		$this->mainBodyWidget = $mainBodyWidget;
	}

	public function render():ResponseItf
	{
		return $this->buildResponse();
	}

	protected function renderMainBodyWidget():string
	{
		return $this->mainBodyWidget->render(
			$this->buildMainBodyWidgetParams()
		);
	}

		private function buildMainBodyWidgetParams():BlogFrontAboutMeMainBodyWidgetParamsItf
		{
			return new BlogFrontAboutMeMainBodyWidgetParams(
				$this->getPageTitle()
			);
		}

	protected function getPageTitleByTranslation(BlogTranslationItf $translation):string
	{
		return $translation->blogFrontAboutMe_pageTitle();
	}

	protected function buildAbsoluteUrlByLanguageCode(string $languageCode):string
	{
		return $this->urlBuilder->buildAbsolute($languageCode);
	}

}