<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\view\widget\mainBody;

use blog\zone\front\page\aboutMe\view\widget\mainBody\BlogFrontAboutMeMainBodyWidgetItf;
use blog\zone\front\page\aboutMe\view\widget\mainBody\BlogFrontAboutMeMainBodyWidgetParamsItf;
use blog\zone\front\page\aboutMe\view\widget\mainBody\BlogFrontAboutMeMainBodyWidgetContext;
use blog\zone\front\page\aboutMe\view\widget\mainBody\BlogFrontAboutMeMainBodyWidgetContextItf;
use blog\widget\BlogTranslatableWidgetAbs;

final class BlogFrontAboutMeMainBodyWidget extends BlogTranslatableWidgetAbs implements BlogFrontAboutMeMainBodyWidgetItf
{

	public function render(BlogFrontAboutMeMainBodyWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(BlogFrontAboutMeMainBodyWidgetParamsItf $params):BlogFrontAboutMeMainBodyWidgetContextItf
		{
			return new BlogFrontAboutMeMainBodyWidgetContext();
		}

		private function renderByContext(BlogFrontAboutMeMainBodyWidgetContextItf $context):string
		{
			$translation = $this->getTranslation();
			ob_start();
			require('blogFrontAboutMeMainBodyWidgetTemplate.php');
			return ob_get_clean();
		}

}