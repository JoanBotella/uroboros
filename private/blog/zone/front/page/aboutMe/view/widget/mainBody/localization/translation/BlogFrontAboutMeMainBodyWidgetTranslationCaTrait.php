<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\view\widget\mainBody\localization\translation;

trait BlogFrontAboutMeMainBodyWidgetTranslationCaTrait
{

	public function blogFrontAboutMeMainBody_content():string
	{
		return <<<END
<p>Esta pàgina està solament en castellà!</p>
END;
	}

}