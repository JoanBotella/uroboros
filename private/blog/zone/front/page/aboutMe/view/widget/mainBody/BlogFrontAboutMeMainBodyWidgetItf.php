<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\view\widget\mainBody;

use blog\zone\front\page\aboutMe\view\widget\mainBody\BlogFrontAboutMeMainBodyWidgetParamsItf;

interface BlogFrontAboutMeMainBodyWidgetItf
{

	public function render(BlogFrontAboutMeMainBodyWidgetParamsItf $params):string;

}