<?php

use blog\zone\front\page\aboutMe\view\widget\mainBody\BlogFrontAboutMeMainBodyWidgetContextItf;
use blog\zone\front\page\aboutMe\view\widget\mainBody\localization\translation\BlogFrontAboutMeMainBodyWidgetTranslationItf;

/**
 * @var BlogFrontAboutMeMainBodyWidgetContextItf $context
 * @var BlogFrontAboutMeMainBodyWidgetTranslationItf $translation
 */

?>	<div class="blog-front-about_meMe-main_body-widget">
<?=$translation->blogFrontAboutMeMainBody_content();?>
	</div>
