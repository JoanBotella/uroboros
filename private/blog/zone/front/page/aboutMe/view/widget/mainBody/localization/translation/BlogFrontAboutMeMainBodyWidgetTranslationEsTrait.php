<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\view\widget\mainBody\localization\translation;

trait BlogFrontAboutMeMainBodyWidgetTranslationEsTrait
{

	public function blogFrontAboutMeMainBody_content():string
	{
		return <<<END
<p>Nací en los ochenta y pocos, pero me habría gustado nacer diez años antes para aprender a programar en un microcomputador y disfrutar como adolescente de esta década prodigiosa. A ver, que los noventa no estuvieron mal, pero puestos a pedir, lo quiero todo.</p>
<p>Soy oriundo de La Vila Joiosa, un pueblecito entre Alicante y Benidorm, pero persiguiendo a mi pareja he vivido en Valencia varios años, después en Sabadell, y ahora mismo estamos en Terrassa. Hacer mudanzas es pesado, pero vivir en distintos lugares es divertido. Excepto por ir dejando amistades y familia atrás, claro. Bajarte al pueblo durante las vacaciones para verlos unas dos veces al año no es lo mismo que tenerlos siempre ahí, disponibles.</p>
<p>Estudié Ingeniería Técnica en Informática de Sistemas en la Universidad de Alicante, pero estaba en un momento complicado de mi vida y tardé muuucho tiempo en acabar la carrera. Cuando conocí a mi pareja me espabilé, y en nuestro nomadismo fui trabajando primero de analista informático y después de desarrollador web. Estuve unos nueve años en el mundo de la empresa privada, con algún pinito como freelance, hasta que me harté de los jefes y del capitalismo en general. Mi pareja me repetía que probara a ser profesor de FP, como ella, que me iba a gustar. Así que probé.</p>
<p>Mi primer curso como docente de FP fue el 19-20, que fue particularmente interesante debido a la pandemia, pero que, a pesar de otras movidas que también me pasaron, me dejó buen sabor de boca. Llevo desde entonces como profesor en la pública, relativamente contento, pero sin olvidar que mi primera vocación es la informática, y concretamente, la programación.</p>
<p>A los trece o catorce años empezaron a gustarme mucho leer libros de fantasía y ciencia ficción, y sobre todo, los juegos de rol. Descubrí que me encantaba inventarme aventuras y escribir historias, y creo que no se me daba mal. Son aficiones que continuan apasionándome, aunque ya no tenga tiempo para casi nada. Cuando tenía dieciséis años, mi madre me llevó a un psicólogo a que me ayudara a decidir entre estudiar informática y algo relacionado con la escritura o crear historias. Siempre me he arrepentido de dejarme influenciar por alguien que apenas me conocía y cuyo razonamiento fué "estudia informática, que ganarás más dinero". ¿Quién sabe cómo sería mi vida hoy en día si hubiera escogido el otro camino? Tal vez más pobre, pero tal vez más realizado.</p>
<p>Otra afición tardía que he descubierto recientemente es la electrónica. Estudié un poco en la carrera, pero de una forma tan teórica y aburrida que la ignoré muy fuerte. Hace algunos años empecé a trastear con Arduinos, y desde que tengo que hacer una introducción a la misma en mis clases de FP, tengo una buena excusa para seguir jugueteando y ampliando conocimientos. Últimamente estoy estoy mezclando la electrónica con la programación, aprendiendo un poco de ensamblador y C para Commodore 64. ¡Muchas aficiones, poco tiempo!</p>
<p>Y menos ahora, que acabo de ser padre.</p>
END;
	}

}