<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\view;

use uroboros\response\ResponseItf;

interface BlogFrontAboutMeViewItf
{

	public function render():ResponseItf;

}