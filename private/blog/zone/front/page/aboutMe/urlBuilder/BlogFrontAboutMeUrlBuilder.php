<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\urlBuilder;

use blog\zone\front\page\aboutMe\urlBuilder\BlogFrontAboutMeUrlBuilderItf;
use blog\urlBuilder\BlogUrlBuilderAbs;

final class BlogFrontAboutMeUrlBuilder extends BlogUrlBuilderAbs implements BlogFrontAboutMeUrlBuilderItf
{

	public function buildRelative(string $languageCode):string
	{
		return $languageCode.'/'.$this->getTranslation($languageCode)->blogFrontAboutMe_slug();
	}

	public function buildAbsolute(string $languageCode):string
	{
		return $this->getBaseUrl().$this->buildRelative($languageCode);
	}

}