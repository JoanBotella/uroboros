<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutMe\urlBuilder;

interface BlogFrontAboutMeUrlBuilderItf
{

	public function buildRelative(string $languageCode):string;

	public function buildAbsolute(string $languageCode):string;

}