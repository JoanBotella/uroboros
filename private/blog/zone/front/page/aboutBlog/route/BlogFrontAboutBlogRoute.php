<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\route;

use blog\zone\front\page\aboutBlog\dependencyInjector\BlogFrontAboutBlogDependencyInjectorItf;
use uroboros\zone\page\route\RouteItf;
use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;

final class BlogFrontAboutBlogRoute implements RouteItf
{

	private BlogFrontAboutBlogDependencyInjectorItf $dependencyInjector;

	public function __construct(
		BlogFrontAboutBlogDependencyInjectorItf $dependencyInjector
	)
	{
		$this->dependencyInjector = $dependencyInjector;
	}

	public function getMatcher():MatcherItf
	{
		return $this->dependencyInjector->getBlogFrontAboutBlogMatcher();
	}

	public function getController():ControllerItf
	{
		return $this->dependencyInjector->getBlogFrontAboutBlogController();
	}

}