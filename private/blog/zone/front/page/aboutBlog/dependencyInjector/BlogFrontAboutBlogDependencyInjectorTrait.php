<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\dependencyInjector;

use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;
use uroboros\view\html\widget\html\HtmlWidgetItf;

use blog\zone\front\page\aboutBlog\matcher\BlogFrontAboutBlogMatcher;
use blog\zone\front\page\aboutBlog\controller\BlogFrontAboutBlogController;
use blog\zone\front\page\aboutBlog\urlBuilder\BlogFrontAboutBlogUrlBuilder;
use blog\zone\front\page\aboutBlog\urlBuilder\BlogFrontAboutBlogUrlBuilderItf;
use blog\zone\front\page\aboutBlog\view\BlogFrontAboutBlogView;
use blog\zone\front\page\aboutBlog\view\BlogFrontAboutBlogViewItf;
use blog\zone\front\page\aboutBlog\view\widget\mainBody\BlogFrontAboutBlogMainBodyWidget;
use blog\zone\front\page\aboutBlog\view\widget\mainBody\BlogFrontAboutBlogMainBodyWidgetItf;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use uroboros\configurationContainer\ConfigurationContainerItf;

trait BlogFrontAboutBlogDependencyInjectorTrait
{

	public function getBlogFrontAboutBlogMatcher():MatcherItf
	{
		return new BlogFrontAboutBlogMatcher(
			$this->getTranslationContainer()
		);
	}

	public function getBlogFrontAboutBlogController():ControllerItf
	{
		return new BlogFrontAboutBlogController(
			$this->getBlogFrontAboutBlogView()
		);
	}

		private function getBlogFrontAboutBlogView():BlogFrontAboutBlogViewItf
		{
			return new BlogFrontAboutBlogView(
				$this->getHtmlWidget(),
				$this->getTranslationContainer(),
				$this->getBlogFrontBodyWidget(),
				$this->getBlogFrontAboutBlogUrlBuilder(),
				$this->getBlogFrontAboutBlogMainBodyWidget()
			);
		}

			abstract private function getHtmlWidget():HtmlWidgetItf;

			abstract private function getTranslationContainer():BlogTranslationContainerItf;

			abstract private function getBlogFrontBodyWidget():BlogFrontBodyWidgetItf;

			abstract private function getBlogFrontBodyHeaderNavWidget():BlogFrontBodyHeaderNavWidgetItf;

			private function getBlogFrontAboutBlogMainBodyWidget():BlogFrontAboutBlogMainBodyWidgetItf
			{
				return new BlogFrontAboutBlogMainBodyWidget(
					$this->getTranslationContainer()
				);
			}

		private function getBlogFrontAboutBlogUrlBuilder():BlogFrontAboutBlogUrlBuilderItf
		{
			return new BlogFrontAboutBlogUrlBuilder(
				$this->getConfigurationContainer(),
				$this->getTranslationContainer()
			);
		}

			abstract private function getConfigurationContainer():ConfigurationContainerItf;

}
