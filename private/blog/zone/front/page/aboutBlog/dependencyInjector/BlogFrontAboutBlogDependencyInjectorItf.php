<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\dependencyInjector;

use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;

interface BlogFrontAboutBlogDependencyInjectorItf
{

	public function getBlogFrontAboutBlogMatcher():MatcherItf;

	public function getBlogFrontAboutBlogController():ControllerItf;

}