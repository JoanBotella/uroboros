<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\view\widget\mainBody\localization\translation;

interface BlogFrontAboutBlogMainBodyWidgetTranslationItf
{

	public function blogFrontAboutBlogMainBody_content():string;

}