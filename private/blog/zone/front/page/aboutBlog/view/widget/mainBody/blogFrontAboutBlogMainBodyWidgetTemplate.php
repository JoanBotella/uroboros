<?php

use blog\zone\front\page\aboutBlog\view\widget\mainBody\BlogFrontAboutBlogMainBodyWidgetContextItf;
use blog\zone\front\page\aboutBlog\view\widget\mainBody\localization\translation\BlogFrontAboutBlogMainBodyWidgetTranslationItf;

/**
 * @var BlogFrontAboutBlogMainBodyWidgetContextItf $context
 * @var BlogFrontAboutBlogMainBodyWidgetTranslationItf $translation
 */

?>	<div class="blog-front-about_blog-main_body-widget">
<?=$translation->blogFrontAboutBlogMainBody_content();?>
	</div>
