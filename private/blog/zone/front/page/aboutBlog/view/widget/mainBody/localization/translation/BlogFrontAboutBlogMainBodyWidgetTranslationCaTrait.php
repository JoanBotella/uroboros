<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\view\widget\mainBody\localization\translation;

trait BlogFrontAboutBlogMainBodyWidgetTranslationCaTrait
{

	public function blogFrontAboutBlogMainBody_content():string
	{
		return <<<END
<p>Esta pàgina està solament en castellà!</p>
END;
	}

}