<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\view\widget\mainBody;

use blog\zone\front\page\aboutBlog\view\widget\mainBody\BlogFrontAboutBlogMainBodyWidgetParamsItf;

interface BlogFrontAboutBlogMainBodyWidgetItf
{

	public function render(BlogFrontAboutBlogMainBodyWidgetParamsItf $params):string;

}