<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\view\widget\mainBody\localization\translation;

trait BlogFrontAboutBlogMainBodyWidgetTranslationEsTrait
{

	public function blogFrontAboutBlogMainBody_content():string
	{
		return <<<END
<p>Hay un dicho en informática, y supongo que en otros campos también, que dice: &quot;no reinventes la rueda&quot;. Viene a significar que si algo ya existe y lo puedes usar, no pierdas tiempo en volver a crearlo tú. Yo le encuentro un grave problema a esta forma de pensar: si siempre construyes a partir del trabajo de otros, nunca sabrás cómo funcionan realmente las cosas; siempre estarás a tan alto nivel que el funcionamiento elemental se te escapará, y siempre dependerás de otros. Hay que poner un límite, claro: no vas a construirte tu propio ordenador y a programar tu propio servidor web para crear una página personal chorra, pero si no te supone un gran esfuerzo, crear un CMS básico en plan WordPress podría servirte para entender cómo funcionan los más avanzados. Supongo que el dicho de la rueda tiene más sentido en un mundo laboral en el que tienes que resultar productivo ASAP, y tu crecimiento personal importa poco. Éste no es el caso.</p>
<figure class="blog-front-about_blog-main_body-widget__uroboros_logo"><img src="asset/blog/zone/front/page/aboutBlog/logo-black.png" /></figure>
<p>Soy una persona que empieza muchos side-projects, pero acaba pocos. Cuando supero el reto de entender cómo funciona la parte técnica, el resto ya me aburre soberanamente. Es por esto que desde que sé programar páginas web he implementado varios frameworks, pero pocas veces los he puesto en producción. En concreto, al código que sostiene este blog lo he llamado &quot;Uroboros&quot;, así en plan épico-interesante, porque me parece que representa bien cómo funciono: antes de acabar una cosa ya he empezado la siguiente. Espero que este nombre me recuerde que <strong>no</strong> quiero que me pase lo mismo esta vez. La idea es ir añadiéndole funcionalidades al blog poco a poco. Empiezo solamente mostrando unas pocas páginas estáticas, después le añadiré formularios, después posts etiquetados que saldrán de una base de datos SQLite, etc.</p>
END;
	}

}