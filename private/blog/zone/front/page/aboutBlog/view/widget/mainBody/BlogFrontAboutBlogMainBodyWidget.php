<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\view\widget\mainBody;

use blog\zone\front\page\aboutBlog\view\widget\mainBody\BlogFrontAboutBlogMainBodyWidgetItf;
use blog\zone\front\page\aboutBlog\view\widget\mainBody\BlogFrontAboutBlogMainBodyWidgetParamsItf;
use blog\zone\front\page\aboutBlog\view\widget\mainBody\BlogFrontAboutBlogMainBodyWidgetContext;
use blog\zone\front\page\aboutBlog\view\widget\mainBody\BlogFrontAboutBlogMainBodyWidgetContextItf;
use blog\widget\BlogTranslatableWidgetAbs;

final class BlogFrontAboutBlogMainBodyWidget extends BlogTranslatableWidgetAbs implements BlogFrontAboutBlogMainBodyWidgetItf
{

	public function render(BlogFrontAboutBlogMainBodyWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(BlogFrontAboutBlogMainBodyWidgetParamsItf $params):BlogFrontAboutBlogMainBodyWidgetContextItf
		{
			return new BlogFrontAboutBlogMainBodyWidgetContext();
		}

		private function renderByContext(BlogFrontAboutBlogMainBodyWidgetContextItf $context):string
		{
			$translation = $this->getTranslation();
			ob_start();
			require('blogFrontAboutBlogMainBodyWidgetTemplate.php');
			return ob_get_clean();
		}

}