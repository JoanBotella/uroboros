<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\view\widget\mainBody;

use blog\zone\front\page\aboutBlog\view\widget\mainBody\BlogFrontAboutBlogMainBodyWidgetParamsItf;

final class BlogFrontAboutBlogMainBodyWidgetParams implements BlogFrontAboutBlogMainBodyWidgetParamsItf
{
}