<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\view;

use uroboros\response\ResponseItf;
use uroboros\view\html\widget\html\HtmlWidgetItf;

use blog\localization\translation\BlogTranslationItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use blog\zone\front\page\aboutBlog\urlBuilder\BlogFrontAboutBlogUrlBuilderItf;
use blog\zone\front\page\aboutBlog\view\BlogFrontAboutBlogViewItf;
use blog\zone\front\page\aboutBlog\view\widget\mainBody\BlogFrontAboutBlogMainBodyWidgetItf;
use blog\zone\front\page\aboutBlog\view\widget\mainBody\BlogFrontAboutBlogMainBodyWidgetParams;
use blog\zone\front\page\aboutBlog\view\widget\mainBody\BlogFrontAboutBlogMainBodyWidgetParamsItf;
use blog\zone\front\view\BlogFrontViewAbs;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;

final class BlogFrontAboutBlogView extends BlogFrontViewAbs implements BlogFrontAboutBlogViewItf
{

	private BlogFrontAboutBlogUrlBuilderItf $urlBuilder;
	private BlogFrontAboutBlogMainBodyWidgetItf $mainBodyWidget;

	public function __construct(
		HtmlWidgetItf $htmlWidget,
		BlogTranslationContainerItf $translationContainer,
		BlogFrontBodyWidgetItf $bodyWidget,
		BlogFrontAboutBlogUrlBuilderItf $urlBuilder,
		BlogFrontAboutBlogMainBodyWidgetItf $mainBodyWidget
	)
	{
		parent::__construct(
			$htmlWidget,
			$translationContainer,
			$bodyWidget
		);
		$this->urlBuilder = $urlBuilder;
		$this->mainBodyWidget = $mainBodyWidget;
	}

	public function render():ResponseItf
	{
		return $this->buildResponse();
	}

	protected function renderMainBodyWidget():string
	{
		return $this->mainBodyWidget->render(
			$this->buildMainBodyWidgetParams()
		);
	}

		private function buildMainBodyWidgetParams():BlogFrontAboutBlogMainBodyWidgetParamsItf
		{
			return new BlogFrontAboutBlogMainBodyWidgetParams(
				$this->getPageTitle()
			);
		}

	protected function getPageTitleByTranslation(BlogTranslationItf $translation):string
	{
		return $translation->blogFrontAboutBlog_pageTitle();
	}

	protected function buildAbsoluteUrlByLanguageCode(string $languageCode):string
	{
		return $this->urlBuilder->buildAbsolute($languageCode);
	}

}