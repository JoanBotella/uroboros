<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\view;

use uroboros\response\ResponseItf;

interface BlogFrontAboutBlogViewItf
{

	public function render():ResponseItf;

}