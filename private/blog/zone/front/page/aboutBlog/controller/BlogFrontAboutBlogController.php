<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\controller;

use uroboros\zone\page\controller\ControllerItf;
use blog\zone\front\page\aboutBlog\view\BlogFrontAboutBlogViewItf;
use uroboros\request\RequestItf;
use uroboros\response\ResponseItf;

final class BlogFrontAboutBlogController implements ControllerItf
{

	private BlogFrontAboutBlogViewItf $view;

	public function __construct(
		BlogFrontAboutBlogViewItf $view
	)
	{
		$this->view = $view;
	}

	public function action(RequestItf $request):ResponseItf
	{
		return $this->view->render();
	}

}