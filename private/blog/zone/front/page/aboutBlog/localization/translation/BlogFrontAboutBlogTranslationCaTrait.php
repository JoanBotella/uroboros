<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\localization\translation;

use blog\zone\front\page\aboutBlog\view\widget\mainBody\localization\translation\BlogFrontAboutBlogMainBodyWidgetTranslationCaTrait;

trait BlogFrontAboutBlogTranslationCaTrait
{
	use
		BlogFrontAboutBlogMainBodyWidgetTranslationCaTrait
	;

	public function blogFrontAboutBlog_slug():string { return 'sobre-el-blog'; }

	public function blogFrontAboutBlog_pageTitle():string { return 'Sobre el Blog'; }

}