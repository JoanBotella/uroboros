<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\localization\translation;

use blog\zone\front\page\aboutBlog\view\widget\mainBody\localization\translation\BlogFrontAboutBlogMainBodyWidgetTranslationItf;

interface BlogFrontAboutBlogTranslationItf
extends
	BlogFrontAboutBlogMainBodyWidgetTranslationItf
{

	public function blogFrontAboutBlog_slug():string;

	public function blogFrontAboutBlog_pageTitle():string;

}