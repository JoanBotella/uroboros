<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\localization\translation;

use blog\zone\front\page\aboutBlog\view\widget\mainBody\localization\translation\BlogFrontAboutBlogMainBodyWidgetTranslationEnTrait;

trait BlogFrontAboutBlogTranslationEnTrait
{
	use
		BlogFrontAboutBlogMainBodyWidgetTranslationEnTrait
	;

	public function blogFrontAboutBlog_slug():string { return 'about-the-blog'; }

	public function blogFrontAboutBlog_pageTitle():string { return 'About the Blog'; }

}