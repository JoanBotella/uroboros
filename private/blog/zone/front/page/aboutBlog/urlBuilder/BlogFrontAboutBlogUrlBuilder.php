<?php
declare(strict_types=1);

namespace blog\zone\front\page\aboutBlog\urlBuilder;

use blog\zone\front\page\aboutBlog\urlBuilder\BlogFrontAboutBlogUrlBuilderItf;
use blog\urlBuilder\BlogUrlBuilderAbs;

final class BlogFrontAboutBlogUrlBuilder extends BlogUrlBuilderAbs implements BlogFrontAboutBlogUrlBuilderItf
{

	public function buildRelative(string $languageCode):string
	{
		return $languageCode.'/'.$this->getTranslation($languageCode)->blogFrontAboutBlog_slug();
	}

	public function buildAbsolute(string $languageCode):string
	{
		return $this->getBaseUrl().$this->buildRelative($languageCode);
	}

}