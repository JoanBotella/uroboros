<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\urlBuilder;

use blog\zone\front\page\notFound\urlBuilder\BlogFrontNotFoundUrlBuilderItf;
use blog\urlBuilder\BlogUrlBuilderAbs;

final class BlogFrontNotFoundUrlBuilder extends BlogUrlBuilderAbs implements BlogFrontNotFoundUrlBuilderItf
{

	public function buildRelative(string $languageCode):string
	{
		return $languageCode.'/'.$this->getTranslation($languageCode)->blogFrontNotFound_slug();
	}

	public function buildAbsolute(string $languageCode):string
	{
		return $this->getBaseUrl().$this->buildRelative($languageCode);
	}

}