<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\controllerProxy;

use blog\zone\front\page\notFound\dependencyInjector\BlogFrontNotFoundDependencyInjectorItf;
use uroboros\zone\page\controller\ControllerItf;
use uroboros\zone\page\controllerProxy\ControllerProxyItf;

final class BlogFrontNotFoundControllerProxy implements ControllerProxyItf
{

	private BlogFrontNotFoundDependencyInjectorItf $dependencyInjector;

	public function __construct(
		BlogFrontNotFoundDependencyInjectorItf $dependencyInjector
	)
	{
		$this->dependencyInjector = $dependencyInjector;
	}

	public function getController():ControllerItf
	{
		return $this->dependencyInjector->getBlogFrontNotFoundController();
	}

}