<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\matcher;

use blog\matcher\BlogMatcherAbs;
use uroboros\zone\page\matcher\MatcherItf;
use uroboros\request\RequestItf;

final class BlogFrontNotFoundMatcher extends BlogMatcherAbs implements MatcherItf
{

	public function match(RequestItf $request):bool
	{
		$translation = $this->getTranslation();
		$pagePath = $translation->getLanguageCode().'/'.$translation->blogFrontNotFound_slug();
		return $request->getPath() == $pagePath;
	}

}