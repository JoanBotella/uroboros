<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\localization\translation;

use blog\zone\front\page\notFound\view\widget\mainBody\localization\translation\BlogFrontNotFoundMainBodyWidgetTranslationCaTrait;

trait BlogFrontNotFoundTranslationCaTrait
{
	use
		BlogFrontNotFoundMainBodyWidgetTranslationCaTrait
	;

	public function blogFrontNotFound_slug():string { return 'no-sha-trobat'; }

	public function blogFrontNotFound_pageTitle():string { return 'No S\'ha Trobat'; }

}