<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\localization\translation;

use blog\zone\front\page\notFound\view\widget\mainBody\localization\translation\BlogFrontNotFoundMainBodyWidgetTranslationItf;

interface BlogFrontNotFoundTranslationItf
extends
	BlogFrontNotFoundMainBodyWidgetTranslationItf
{

	public function blogFrontNotFound_slug():string;

	public function blogFrontNotFound_pageTitle():string;

}