<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\localization\translation;

use blog\zone\front\page\notFound\view\widget\mainBody\localization\translation\BlogFrontNotFoundMainBodyWidgetTranslationEsTrait;

trait BlogFrontNotFoundTranslationEsTrait
{
	use
		BlogFrontNotFoundMainBodyWidgetTranslationEsTrait
	;

	public function blogFrontNotFound_slug():string { return 'no-se-ha-encontrado'; }

	public function blogFrontNotFound_pageTitle():string { return 'No Se Ha Encontrado'; }

}