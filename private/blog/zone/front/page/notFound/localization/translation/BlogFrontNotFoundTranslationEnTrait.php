<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\localization\translation;

use blog\zone\front\page\notFound\view\widget\mainBody\localization\translation\BlogFrontNotFoundMainBodyWidgetTranslationEnTrait;

trait BlogFrontNotFoundTranslationEnTrait
{
	use
		BlogFrontNotFoundMainBodyWidgetTranslationEnTrait
	;

	public function blogFrontNotFound_slug():string { return 'not-found'; }

	public function blogFrontNotFound_pageTitle():string { return 'Not Found'; }

}