<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\dependencyInjector;

use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;
use uroboros\view\html\widget\html\HtmlWidgetItf;

use blog\zone\front\page\notFound\matcher\BlogFrontNotFoundMatcher;
use blog\zone\front\page\notFound\controller\BlogFrontNotFoundController;
use blog\zone\front\page\notFound\controllerProxy\BlogFrontNotFoundControllerProxy;
use blog\zone\front\page\notFound\urlBuilder\BlogFrontNotFoundUrlBuilder;
use blog\zone\front\page\notFound\urlBuilder\BlogFrontNotFoundUrlBuilderItf;
use blog\zone\front\page\notFound\view\BlogFrontNotFoundView;
use blog\zone\front\page\notFound\view\BlogFrontNotFoundViewItf;
use blog\zone\front\page\notFound\view\widget\mainBody\BlogFrontNotFoundMainBodyWidget;
use blog\zone\front\page\notFound\view\widget\mainBody\BlogFrontNotFoundMainBodyWidgetItf;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;
use blog\zone\front\view\widget\body\widget\header\widget\nav\BlogFrontBodyHeaderNavWidgetItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use uroboros\zone\page\controllerProxy\ControllerProxyItf;
use uroboros\configurationContainer\ConfigurationContainerItf;

trait BlogFrontNotFoundDependencyInjectorTrait
{

	public function getBlogFrontNotFoundMatcher():MatcherItf
	{
		return new BlogFrontNotFoundMatcher(
			$this->getTranslationContainer()
		);
	}

	public function getBlogFrontNotFoundController():ControllerItf
	{
		return new BlogFrontNotFoundController(
			$this->getBlogFrontNotFoundView()
		);
	}

		private function getBlogFrontNotFoundView():BlogFrontNotFoundViewItf
		{
			return new BlogFrontNotFoundView(
				$this->getHtmlWidget(),
				$this->getTranslationContainer(),
				$this->getBlogFrontBodyWidget(),
				$this->getBlogFrontNotFoundUrlBuilder(),
				$this->getBlogFrontNotFoundMainBodyWidget()
			);
		}

			abstract private function getHtmlWidget():HtmlWidgetItf;

			abstract private function getTranslationContainer():BlogTranslationContainerItf;

			abstract private function getBlogFrontBodyWidget():BlogFrontBodyWidgetItf;

			abstract private function getBlogFrontBodyHeaderNavWidget():BlogFrontBodyHeaderNavWidgetItf;

			private function getBlogFrontNotFoundMainBodyWidget():BlogFrontNotFoundMainBodyWidgetItf
			{
				return new BlogFrontNotFoundMainBodyWidget(
					$this->getTranslationContainer()
				);
			}

		private function getBlogFrontNotFoundUrlBuilder():BlogFrontNotFoundUrlBuilderItf
		{
			return new BlogFrontNotFoundUrlBuilder(
				$this->getConfigurationContainer(),
				$this->getTranslationContainer()
			);
		}

			abstract private function getConfigurationContainer():ConfigurationContainerItf;
	
	private function getBlogFrontNotFoundControllerProxy():ControllerProxyItf
	{
		return new BlogFrontNotFoundControllerProxy(
			$this
		);
	}

}
