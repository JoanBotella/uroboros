<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\dependencyInjector;

use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;

interface BlogFrontNotFoundDependencyInjectorItf
{

	public function getBlogFrontNotFoundMatcher():MatcherItf;

	public function getBlogFrontNotFoundController():ControllerItf;

}