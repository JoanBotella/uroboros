<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\controller;

use uroboros\zone\page\controller\ControllerItf;
use blog\zone\front\page\notFound\view\BlogFrontNotFoundViewItf;
use uroboros\request\RequestItf;
use uroboros\response\ResponseItf;

final class BlogFrontNotFoundController implements ControllerItf
{

	private BlogFrontNotFoundViewItf $view;

	public function __construct(
		BlogFrontNotFoundViewItf $view
	)
	{
		$this->view = $view;
	}

	public function action(RequestItf $request):ResponseItf
	{
		return $this->view->render();
	}

}