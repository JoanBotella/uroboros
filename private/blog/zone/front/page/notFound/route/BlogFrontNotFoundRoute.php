<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\route;

use blog\zone\front\page\notFound\dependencyInjector\BlogFrontNotFoundDependencyInjectorItf;
use uroboros\zone\page\route\RouteItf;
use uroboros\zone\page\matcher\MatcherItf;
use uroboros\zone\page\controller\ControllerItf;

final class BlogFrontNotFoundRoute implements RouteItf
{

	private BlogFrontNotFoundDependencyInjectorItf $dependencyInjector;

	public function __construct(
		BlogFrontNotFoundDependencyInjectorItf $dependencyInjector
	)
	{
		$this->dependencyInjector = $dependencyInjector;
	}

	public function getMatcher():MatcherItf
	{
		return $this->dependencyInjector->getBlogFrontNotFoundMatcher();
	}

	public function getController():ControllerItf
	{
		return $this->dependencyInjector->getBlogFrontNotFoundController();
	}

}