<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\view\widget\mainBody\localization\translation;

trait BlogFrontNotFoundMainBodyWidgetTranslationCaTrait
{

	public function blogFrontNotFoundMainBody_content():string
	{
		return <<<END
<p>Ups! Pareix que l'adreça a la que has accedit no existeix. Tal volta l'has escrit malament, o pot ser que ja no esté disponible.</p>
END;
	}

}