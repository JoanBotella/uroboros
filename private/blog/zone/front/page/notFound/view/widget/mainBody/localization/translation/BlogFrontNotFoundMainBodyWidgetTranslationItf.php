<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\view\widget\mainBody\localization\translation;

interface BlogFrontNotFoundMainBodyWidgetTranslationItf
{

	public function blogFrontNotFoundMainBody_content():string;

}