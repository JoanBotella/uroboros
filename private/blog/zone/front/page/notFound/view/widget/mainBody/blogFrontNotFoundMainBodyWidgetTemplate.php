<?php

use blog\zone\front\page\notFound\view\widget\mainBody\BlogFrontNotFoundMainBodyWidgetContextItf;
use blog\zone\front\page\notFound\view\widget\mainBody\localization\translation\BlogFrontNotFoundMainBodyWidgetTranslationItf;

/**
 * @var BlogFrontNotFoundMainBodyWidgetContextItf $context
 * @var BlogFrontNotFoundMainBodyWidgetTranslationItf $translation
 */

?>	<div class="blog-front-not_found-main_body-widget">
<?=$translation->blogFrontNotFoundMainBody_content();?>
	</div>
