<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\view\widget\mainBody;

use blog\zone\front\page\notFound\view\widget\mainBody\BlogFrontNotFoundMainBodyWidgetContextItf;

final class BlogFrontNotFoundMainBodyWidgetContext implements BlogFrontNotFoundMainBodyWidgetContextItf
{
}