<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\view\widget\mainBody\localization\translation;

trait BlogFrontNotFoundMainBodyWidgetTranslationEnTrait
{

	public function blogFrontNotFoundMainBody_content():string
	{
		return <<<END
<p>Ups! It looks that the address you just accessed does not exist. Possibly you had a typo writing it down, or maybe it's no longer available.</p>
END;
	}

}