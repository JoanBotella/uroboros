<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\view\widget\mainBody;

use blog\zone\front\page\notFound\view\widget\mainBody\BlogFrontNotFoundMainBodyWidgetItf;
use blog\zone\front\page\notFound\view\widget\mainBody\BlogFrontNotFoundMainBodyWidgetParamsItf;
use blog\zone\front\page\notFound\view\widget\mainBody\BlogFrontNotFoundMainBodyWidgetContext;
use blog\zone\front\page\notFound\view\widget\mainBody\BlogFrontNotFoundMainBodyWidgetContextItf;
use blog\widget\BlogTranslatableWidgetAbs;

final class BlogFrontNotFoundMainBodyWidget extends BlogTranslatableWidgetAbs implements BlogFrontNotFoundMainBodyWidgetItf
{

	public function render(BlogFrontNotFoundMainBodyWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(BlogFrontNotFoundMainBodyWidgetParamsItf $params):BlogFrontNotFoundMainBodyWidgetContextItf
		{
			return new BlogFrontNotFoundMainBodyWidgetContext();
		}

		private function renderByContext(BlogFrontNotFoundMainBodyWidgetContextItf $context):string
		{
			$translation = $this->getTranslation();
			ob_start();
			require('blogFrontNotFoundMainBodyWidgetTemplate.php');
			return ob_get_clean();
		}

}