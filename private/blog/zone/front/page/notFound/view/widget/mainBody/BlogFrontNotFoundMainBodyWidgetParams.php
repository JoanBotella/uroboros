<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\view\widget\mainBody;

use blog\zone\front\page\notFound\view\widget\mainBody\BlogFrontNotFoundMainBodyWidgetParamsItf;

final class BlogFrontNotFoundMainBodyWidgetParams implements BlogFrontNotFoundMainBodyWidgetParamsItf
{
}