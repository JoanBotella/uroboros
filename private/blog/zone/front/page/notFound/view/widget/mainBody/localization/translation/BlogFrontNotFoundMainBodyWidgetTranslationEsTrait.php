<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\view\widget\mainBody\localization\translation;

trait BlogFrontNotFoundMainBodyWidgetTranslationEsTrait
{

	public function blogFrontNotFoundMainBody_content():string
	{
		return <<<END
<p>¡Ups! Parece que la dirección a la que has accedido no existe. Tal vez la has escrito mal, o puede que ya no esté disponible.</p>
END;
	}

}