<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\view;

use uroboros\response\ResponseItf;
use uroboros\view\html\widget\html\HtmlWidgetItf;

use blog\localization\translation\BlogTranslationItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;
use blog\zone\front\page\notFound\urlBuilder\BlogFrontNotFoundUrlBuilderItf;
use blog\zone\front\page\notFound\view\BlogFrontNotFoundViewItf;
use blog\zone\front\page\notFound\view\widget\mainBody\BlogFrontNotFoundMainBodyWidgetItf;
use blog\zone\front\page\notFound\view\widget\mainBody\BlogFrontNotFoundMainBodyWidgetParams;
use blog\zone\front\page\notFound\view\widget\mainBody\BlogFrontNotFoundMainBodyWidgetParamsItf;
use blog\zone\front\view\BlogFrontViewAbs;
use blog\zone\front\view\widget\body\BlogFrontBodyWidgetItf;

final class BlogFrontNotFoundView extends BlogFrontViewAbs implements BlogFrontNotFoundViewItf
{

	private BlogFrontNotFoundUrlBuilderItf $urlBuilder;
	private BlogFrontNotFoundMainBodyWidgetItf $mainBodyWidget;

	public function __construct(
		HtmlWidgetItf $htmlWidget,
		BlogTranslationContainerItf $translationContainer,
		BlogFrontBodyWidgetItf $bodyWidget,
		BlogFrontNotFoundUrlBuilderItf $urlBuilder,
		BlogFrontNotFoundMainBodyWidgetItf $mainBodyWidget
	)
	{
		parent::__construct(
			$htmlWidget,
			$translationContainer,
			$bodyWidget
		);
		$this->urlBuilder = $urlBuilder;
		$this->mainBodyWidget = $mainBodyWidget;
	}

	public function render():ResponseItf
	{
		return $this->buildResponse();
	}

	protected function renderMainBodyWidget():string
	{
		return $this->mainBodyWidget->render(
			$this->buildMainBodyWidgetParams()
		);
	}

		private function buildMainBodyWidgetParams():BlogFrontNotFoundMainBodyWidgetParamsItf
		{
			return new BlogFrontNotFoundMainBodyWidgetParams(
				$this->getPageTitle()
			);
		}

	protected function getPageTitleByTranslation(BlogTranslationItf $translation):string
	{
		return $translation->blogFrontNotFound_pageTitle();
	}

	protected function buildAbsoluteUrlByLanguageCode(string $languageCode):string
	{
		return $this->urlBuilder->buildAbsolute($languageCode);
	}

}