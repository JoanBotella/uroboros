<?php
declare(strict_types=1);

namespace blog\zone\front\page\notFound\view;

use uroboros\response\ResponseItf;

interface BlogFrontNotFoundViewItf
{

	public function render():ResponseItf;

}