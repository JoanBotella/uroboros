<?php
declare(strict_types=1);

namespace blog\zone\front\localization\translation;

use blog\zone\front\page\aboutMe\localization\translation\BlogFrontAboutMeTranslationEnTrait;
use blog\zone\front\page\aboutBlog\localization\translation\BlogFrontAboutBlogTranslationEnTrait;
use blog\zone\front\page\home\localization\translation\BlogFrontHomeTranslationEnTrait;
use blog\zone\front\page\notFound\localization\translation\BlogFrontNotFoundTranslationEnTrait;
use blog\zone\front\page\contact\localization\translation\BlogFrontContactTranslationEnTrait;
use blog\zone\front\view\widget\body\localization\translation\BlogFrontBodyWidgetTranslationEnTrait;

trait BlogFrontTranslationEnTrait
{
	use
		BlogFrontAboutMeTranslationEnTrait,
		BlogFrontAboutBlogTranslationEnTrait,
		BlogFrontHomeTranslationEnTrait,
		BlogFrontNotFoundTranslationEnTrait,
		BlogFrontContactTranslationEnTrait,

		BlogFrontBodyWidgetTranslationEnTrait
	;
}