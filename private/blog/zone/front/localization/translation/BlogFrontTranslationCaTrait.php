<?php
declare(strict_types=1);

namespace blog\zone\front\localization\translation;

use blog\zone\front\page\aboutMe\localization\translation\BlogFrontAboutMeTranslationCaTrait;
use blog\zone\front\page\aboutBlog\localization\translation\BlogFrontAboutBlogTranslationCaTrait;
use blog\zone\front\page\home\localization\translation\BlogFrontHomeTranslationCaTrait;
use blog\zone\front\page\notFound\localization\translation\BlogFrontNotFoundTranslationCaTrait;
use blog\zone\front\page\contact\localization\translation\BlogFrontContactTranslationCaTrait;
use blog\zone\front\view\widget\body\localization\translation\BlogFrontBodyWidgetTranslationCaTrait;

trait BlogFrontTranslationCaTrait
{
	use
		BlogFrontAboutMeTranslationCaTrait,
		BlogFrontAboutBlogTranslationCaTrait,
		BlogFrontHomeTranslationCaTrait,
		BlogFrontNotFoundTranslationCaTrait,
		BlogFrontContactTranslationCaTrait,

		BlogFrontBodyWidgetTranslationCaTrait
	;
}