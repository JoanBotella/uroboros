<?php
declare(strict_types=1);

namespace blog\zone\front\localization\translation;

use blog\zone\front\page\aboutMe\localization\translation\BlogFrontAboutMeTranslationItf;
use blog\zone\front\page\aboutBlog\localization\translation\BlogFrontAboutBlogTranslationItf;
use blog\zone\front\page\contact\localization\translation\BlogFrontContactTranslationItf;
use blog\zone\front\page\home\localization\translation\BlogFrontHomeTranslationItf;
use blog\zone\front\page\notFound\localization\translation\BlogFrontNotFoundTranslationItf;
use blog\zone\front\view\widget\body\localization\translation\BlogFrontBodyWidgetTranslationItf;

interface BlogFrontTranslationItf
extends
	BlogFrontAboutMeTranslationItf,
	BlogFrontAboutBlogTranslationItf,
	BlogFrontHomeTranslationItf,
	BlogFrontNotFoundTranslationItf,
	BlogFrontContactTranslationItf,

	BlogFrontBodyWidgetTranslationItf
{
}