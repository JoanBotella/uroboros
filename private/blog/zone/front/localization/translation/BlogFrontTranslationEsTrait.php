<?php
declare(strict_types=1);

namespace blog\zone\front\localization\translation;

use blog\zone\front\page\aboutMe\localization\translation\BlogFrontAboutMeTranslationEsTrait;
use blog\zone\front\page\aboutBlog\localization\translation\BlogFrontAboutBlogTranslationEsTrait;
use blog\zone\front\page\home\localization\translation\BlogFrontHomeTranslationEsTrait;
use blog\zone\front\page\notFound\localization\translation\BlogFrontNotFoundTranslationEsTrait;
use blog\zone\front\page\contact\localization\translation\BlogFrontContactTranslationEsTrait;
use blog\zone\front\view\widget\body\localization\translation\BlogFrontBodyWidgetTranslationEsTrait;

trait BlogFrontTranslationEsTrait
{
	use
		BlogFrontAboutMeTranslationEsTrait,
		BlogFrontAboutBlogTranslationEsTrait,
		BlogFrontHomeTranslationEsTrait,
		BlogFrontNotFoundTranslationEsTrait,
		BlogFrontContactTranslationEsTrait,

		BlogFrontBodyWidgetTranslationEsTrait
	;
}