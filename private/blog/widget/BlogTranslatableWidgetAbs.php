<?php
declare(strict_types=1);

namespace blog\widget;

use blog\localization\translation\BlogTranslationItf;
use blog\localization\translationContainer\BlogTranslationContainerItf;

abstract class BlogTranslatableWidgetAbs
{

	private BlogTranslationContainerItf $translationContainer;

	public function __construct(
		BlogTranslationContainerItf $translationContainer
	)
	{
		$this->translationContainer = $translationContainer;
	}

	protected function getTranslation():BlogTranslationItf
	{
		return $this->translationContainer->get();
	}

	protected function getTranslationByLanguageCode(string $languageCode):BlogTranslationItf
	{
		return $this->translationContainer->getByLanguageCode($languageCode);
	}

	protected function getLanguageCode():string
	{
		return $this->getTranslation()->getLanguageCode();
	}

}