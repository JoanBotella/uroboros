<?php
declare(strict_types=1);

namespace uroboros\mimeType;

final class MimeTypeConstant
{

	const PNG = 'image/png';

}