<?php
declare(strict_types=1);

namespace uroboros\response;

interface ResponseItf
{

	public function getHttpStatusCode():int;

	public function getContent():string;

}