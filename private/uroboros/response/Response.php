<?php
declare(strict_types=1);

namespace uroboros\response;

use uroboros\response\ResponseItf;

final class Response implements ResponseItf
{

	private int $httpStatusCode;
	private string $content;

	public function __construct(
		int $httpStatusCode,
		string $content
	)
	{
		$this->httpStatusCode = $httpStatusCode;
		$this->content = $content;
	}

	public function getHttpStatusCode():int
	{
		return $this->httpStatusCode;
	}

	public function getContent():string
	{
		return $this->content;
	}

}