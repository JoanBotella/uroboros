<?php
declare(strict_types=1);

namespace uroboros\request;

interface RequestItf
{

	public function getPath():string;

	/**
	 * @return array<string>
	 */
	public function getPathSegments():array;

	/**
	 * @return array<string>
	 */
	public function getAcceptedLanguageCodes():array;

}