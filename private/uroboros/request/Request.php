<?php
declare(strict_types=1);

namespace uroboros\request;

use uroboros\request\RequestItf;

final class Request implements RequestItf
{

	private string $path;
	/**
	 * @var array<string> $pathSegments
	 */
	private array $pathSegments;
	/**
	 * @var array<string> $acceptedLanguageCodes
	 */
	private array $acceptedLanguageCodes;

	/**
	 * @param array<string> $pathSegments
	 * @param array<string> $acceptedLanguageCodes
	 */
	public function __construct(
		string $path,
		array $pathSegments,
		array $acceptedLanguageCodes
	)
	{
		$this->path = $path;
		$this->pathSegments = $pathSegments;
		$this->acceptedLanguageCodes = $acceptedLanguageCodes;
	}

	public function getPath():string
	{
		return $this->path;
	}

	/**
	 * @return array<string>
	 */
	public function getPathSegments():array
	{
		return $this->pathSegments;
	}

	/**
	 * @return array<string>
	 */
	public function getAcceptedLanguageCodes():array
	{
		return $this->acceptedLanguageCodes;
	}

}