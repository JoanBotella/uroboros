<?php
declare(strict_types=1);

namespace uroboros\http;

final class HttpConstant
{

	const STATUS_CODE_OK = 200;

	const STATUS_CODE_NOT_FOUND = 404;

}