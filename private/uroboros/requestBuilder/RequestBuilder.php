<?php
declare(strict_types=1);

namespace uroboros\requestBuilder;

use uroboros\request\Request;
use uroboros\request\RequestItf;
use uroboros\superglobalServerWrapper\SuperglobalServerWrapperItf;

final class RequestBuilder implements RequestBuilderItf
{

	private SuperglobalServerWrapperItf $superglobalServerWrapper;

	public function __construct(
		SuperglobalServerWrapperItf $superglobalServerWrapper
	)
	{
		$this->superglobalServerWrapper = $superglobalServerWrapper;
	}

	public function build():RequestItf
	{
		$path = $this->buildPath();
		return new Request(
			$path,
			$this->buildPathSegments($path),
			$this->buildAcceptedLanguageCodes()
		);
	}

		private function buildPath():string
		{
			$path = $this->superglobalServerWrapper->getRequestUri();

			if (substr($path, 0, 1) === '/')
			{
				$path = substr($path, 1);
			}

			if (substr($path, -1) === '/')
			{
				$path = substr($path, 0, -1);
			}

			$path = str_replace('uroboros/repo', '', $path);	// !!!

			if (substr($path, 0, 1) === '/')
			{
				$path = substr($path, 1);
			}

			return $path;
		}

		/**
		 * @return array<string>
		 */
		private function buildPathSegments(string $path):array
		{
			$segments = [];
			$dirtySegments = explode('/', $path);
			/**
			 * @var string $segment
			 */
			foreach ($dirtySegments as $segment)
			{
				if ($segment != '')
				{
					$segments[] = $segment;
				}
			}
			return $segments;
		}

		/**
		 * @return array<string>
		 */
		private function buildAcceptedLanguageCodes():array
		{
			$acceptedLanguageCodes = [];
			foreach ($this->buildLanguageCodesMap() as $languageCode => $languageAndCountryCodes)
			{
				foreach ($languageAndCountryCodes as $languageAndCountryCode)
				{
					$acceptedLanguageCodes[] = $languageAndCountryCode;
				}
				$acceptedLanguageCodes[] = $languageCode;
			}

			return $acceptedLanguageCodes;
		}

			private function buildLanguageCodesMap():array
			{
				// [HTTP_ACCEPT_LANGUAGE] => en-US,en;q=0.8,ca-valencia;q=0.7,ca;q=0.5,es-ES;q=0.3,es;q=0.2

				$string = preg_replace('/[q;=\d\.\s]/', '', $this->superglobalServerWrapper->getHttpAcceptLanguage());
				$split = explode(',', $string);
	
				$map = [];
				foreach ($split as $item)
				{
					$dashIndex = strpos($item, '-');
	
					if ($dashIndex === false)
					{
						if (!array_key_exists($item, $map))
						{
							$map[$item] = [];
						}
						continue;
					}
	
					$languageCode = substr($item, 0, $dashIndex);
	
					if (!array_key_exists($languageCode, $map))
					{
						$map[$languageCode] = [];
					}
	
					$map[$languageCode][] = $item;
				}

				return $map;
			}

}