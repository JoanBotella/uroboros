<?php
declare(strict_types=1);

namespace uroboros\requestBuilder;

use uroboros\request\RequestItf;

interface RequestBuilderItf
{

	public function build():RequestItf;

}