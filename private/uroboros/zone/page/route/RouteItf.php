<?php
declare(strict_types=1);

namespace uroboros\zone\page\route;

use uroboros\zone\page\controller\ControllerItf;
use uroboros\zone\page\matcher\MatcherItf;

interface RouteItf
{

	public function getMatcher():MatcherItf;

	public function getController():ControllerItf;

}