<?php
declare(strict_types=1);

namespace uroboros\zone\page\controller;

use uroboros\request\RequestItf;
use uroboros\response\ResponseItf;

interface ControllerItf
{

	public function action(RequestItf $request):ResponseItf;

}