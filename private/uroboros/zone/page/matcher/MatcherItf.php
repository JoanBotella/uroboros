<?php
declare(strict_types=1);

namespace uroboros\zone\page\matcher;

use uroboros\request\RequestItf;

interface MatcherItf
{

	public function match(RequestItf $request):bool;

}