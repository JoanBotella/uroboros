<?php
declare(strict_types=1);

namespace uroboros\zone\page\controllerProxy;

use uroboros\zone\page\controller\ControllerItf;

interface ControllerProxyItf
{

	public function getController():ControllerItf;

}