<?php
declare(strict_types=1);

namespace uroboros\zone\routeGenerator;

use uroboros\zone\page\route\RouteItf;
use Generator;

interface RouteGeneratorItf
{

	/**
	 * @return Generator<RouteItf>
	 */
	public function generate():Generator;

}