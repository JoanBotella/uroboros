<?php
declare(strict_types=1);

namespace uroboros\zone\routeGenerator;

use Generator;

use uroboros\zone\page\route\RouteItf;
use uroboros\zone\routeGenerator\RouteGeneratorItf;

final class RouteGenerator implements RouteGeneratorItf
{

	/**
	 * @var array<RouteGeneratorItf> $routeGenerators
	 */
	private array $routeGenerators;

	/**
	 * @param array<RouteGeneratorItf> $routeGenerators
	 */
	public function __construct(
		array $routeGenerators
	)
	{
		$this->routeGenerators = $routeGenerators;
	}

	/**
	 * @return Generator<RouteItf>
	 */
	public function generate():Generator
	{
		/**
		 * @var RouteGeneratorItf $routeGenerator
		 */
		foreach($this->routeGenerators as $routeGenerator)
		{
			/**
			 * @var Generator<RouteItf> $route
			 */
			foreach($routeGenerator->generate() as $route)
			{
				yield $route;
			}
		}
	}

}