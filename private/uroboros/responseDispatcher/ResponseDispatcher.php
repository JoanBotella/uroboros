<?php
declare(strict_types=1);

namespace uroboros\responseDispatcher;

use uroboros\responseDispatcher\ResponseDispatcherItf;
use uroboros\response\ResponseItf;

final class ResponseDispatcher implements ResponseDispatcherItf
{

	public function dispatch(ResponseItf $response):void
	{
		http_response_code($response->getHttpStatusCode());
		echo $response->getContent();	// !!!
	}

}