<?php
declare(strict_types=1);

namespace uroboros\responseDispatcher;

use uroboros\response\ResponseItf;

interface ResponseDispatcherItf
{

	public function dispatch(ResponseItf $response):void;

}