<?php
declare(strict_types=1);

namespace uroboros\dependencyInjector;

use uroboros\app\AppItf;

interface DependencyInjectorItf
{

	public function getApp():AppItf;

}