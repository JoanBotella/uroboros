<?php
declare(strict_types=1);

namespace uroboros\dependencyInjector;

use uroboros\app\App;
use uroboros\app\AppItf;
use uroboros\configurationContainer\ConfigurationContainerItf;
use uroboros\localization\translationSetupper\TranslationSetupperItf;
use uroboros\requestBuilder\RequestBuilder;
use uroboros\requestBuilder\RequestBuilderItf;
use uroboros\responseDispatcher\ResponseDispatcher;
use uroboros\responseDispatcher\ResponseDispatcherItf;
use uroboros\router\RouterItf;
use uroboros\superglobalServerWrapper\SuperglobalServerWrapper;
use uroboros\superglobalServerWrapper\SuperglobalServerWrapperItf;
use uroboros\view\html\widget\html\HtmlWidget;
use uroboros\view\html\widget\html\HtmlWidgetItf;

trait DependencyInjectorTrait
{

	public function getApp():AppItf
	{
		return new App(
			$this->getRequestBuilder(),
			$this->getTranslationSetupper(),
			$this->getRouter(),
			$this->getResponseDispatcher()
		);
	}

		private function getRequestBuilder():RequestBuilderItf
		{
			return new RequestBuilder(
				$this->getSuperglobalServerWrapper()
			);
		}

			private function getSuperglobalServerWrapper():SuperglobalServerWrapperItf
			{
				return new SuperglobalServerWrapper();
			}

		abstract private function getTranslationSetupper():TranslationSetupperItf;

		abstract private function getRouter():RouterItf;

		private function getResponseDispatcher():ResponseDispatcherItf
		{
			return new ResponseDispatcher();
		}

	private function getHtmlWidget():HtmlWidgetItf
	{
		return new HtmlWidget(
			$this->getConfigurationContainer()
		);
	}

		abstract private function getConfigurationContainer():ConfigurationContainerItf;

}