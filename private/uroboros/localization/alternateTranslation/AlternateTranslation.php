<?php
declare(strict_types=1);

namespace uroboros\localization\alternateTranslation;

use uroboros\localization\alternateTranslation\AlternateTranslationItf;

final class AlternateTranslation implements AlternateTranslationItf
{

	private string $languageCode;
	private string $languageName;
	private string $pageTitle;
	private string $anchorTitle;
	private string $url;

	public function __construct(
		string $languageCode,
		string $languageName,
		string $pageTitle,
		string $anchorTitle,
		string $url
	)
	{
		$this->languageCode = $languageCode;
		$this->languageName = $languageName;
		$this->pageTitle = $pageTitle;
		$this->anchorTitle = $anchorTitle;
		$this->url = $url;
	}

	public function getLanguageCode():string
	{
		return $this->languageCode;
	}

	public function getLanguageName():string
	{
		return $this->languageName;
	}

	public function getPageTitle():string
	{
		return $this->pageTitle;
	}

	public function getAnchorTitle():string
	{
		return $this->anchorTitle;
	}

	public function getUrl():string
	{
		return $this->url;
	}

}