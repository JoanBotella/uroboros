<?php
declare(strict_types=1);

namespace uroboros\localization\alternateTranslation;

interface AlternateTranslationItf
{

	public function getLanguageCode():string;

	public function getLanguageName():string;

	public function getPageTitle():string;

	public function getAnchorTitle():string;

	public function getUrl():string;

}