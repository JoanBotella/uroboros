<?php
declare(strict_types=1);

namespace uroboros\localization\translationSetupper;

use uroboros\request\RequestItf;

interface TranslationSetupperItf
{

	public function setup(RequestItf $request):void;

}