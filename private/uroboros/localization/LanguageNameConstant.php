<?php
declare(strict_types=1);

namespace uroboros\localization;

final class LanguageNameConstant
{
	const ENGLISH = 'English';
	const CASTILLIAN = 'Castellano';
	const CATALAN = 'Català';
}