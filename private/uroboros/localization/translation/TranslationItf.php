<?php
declare(strict_types=1);

namespace uroboros\localization\translation;

interface TranslationItf
{

	public function getLanguageCode():string;

}