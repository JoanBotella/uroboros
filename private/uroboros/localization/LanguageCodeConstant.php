<?php
declare(strict_types=1);

namespace uroboros\localization;

final class LanguageCodeConstant
{
	const ENGLISH = 'en';
	const CASTILLIAN = 'es';
	const CATALAN = 'ca';
}