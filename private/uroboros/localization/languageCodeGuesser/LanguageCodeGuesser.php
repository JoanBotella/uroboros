<?php
declare(strict_types=1);

namespace uroboros\localization\languageCodeGuesser;

use uroboros\configurationContainer\ConfigurationContainerItf;
use uroboros\localization\languageCodeGuesser\LanguageCodeGuesserItf;
use uroboros\request\RequestItf;

final class LanguageCodeGuesser implements LanguageCodeGuesserItf
{

	private ConfigurationContainerItf $configurationContainer;

	public function __construct(
		ConfigurationContainerItf $configurationContainer
	)
	{
		$this->configurationContainer = $configurationContainer;
	}

	public function guess(RequestItf $request):string
	{
		$acceptedLanguageCodes = $request->getAcceptedLanguageCodes();
		$configuration = $this->configurationContainer->get();
		$enabledLanguageCodes = $configuration->getEnabledLanguageCodes();

		$pathSegments = $request->getPathSegments();

		if (count($pathSegments) > 0)
		{
			if (in_array($pathSegments[0], $enabledLanguageCodes))
			{
				return $pathSegments[0];
			}
		}

		/**
		 * @var string $acceptedLanguageCode
		 */
		foreach ($acceptedLanguageCodes as $acceptedLanguageCode)
		{
			if (in_array($acceptedLanguageCode, $enabledLanguageCodes))
			{
				return $acceptedLanguageCode;
			}
		}

		return $configuration->getDefaultLanguageCode();
	}

}