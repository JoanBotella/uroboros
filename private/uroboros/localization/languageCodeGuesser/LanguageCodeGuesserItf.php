<?php
declare(strict_types=1);

namespace uroboros\localization\languageCodeGuesser;

use uroboros\request\RequestItf;

interface LanguageCodeGuesserItf
{

	public function guess(RequestItf $request):string;

}