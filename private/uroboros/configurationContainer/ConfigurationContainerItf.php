<?php
declare(strict_types=1);

namespace uroboros\configurationContainer;

use uroboros\configuration\ConfigurationItf;

interface ConfigurationContainerItf
{

	public function get():ConfigurationItf;

}