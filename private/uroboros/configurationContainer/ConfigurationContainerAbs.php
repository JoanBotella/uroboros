<?php
declare(strict_types=1);

namespace uroboros\configurationContainer;

use uroboros\configuration\ConfigurationItf;

abstract class ConfigurationContainerAbs
{
	
	private ConfigurationItf $configuration;

	public function get():ConfigurationItf
	{
		if (!isset($this->configuration))
		{
			$this->configuration = $this->build();
		}
		return $this->configuration;
	}

		abstract protected function build():ConfigurationItf;

}