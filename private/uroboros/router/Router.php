<?php
declare(strict_types=1);

namespace uroboros\router;

use uroboros\router\RouterItf;
use uroboros\request\RequestItf;
use uroboros\zone\page\controller\ControllerItf;
use uroboros\zone\page\controllerProxy\ControllerProxyItf;
use uroboros\zone\routeGenerator\RouteGeneratorItf;
use uroboros\zone\page\route\RouteItf;
use uroboros\routeGeneratorsGenerator\RouteGeneratorsGeneratorItf;

final class Router implements RouterItf
{

	private RouteGeneratorsGeneratorItf $routeGeneratorsGenerator;
	private ControllerProxyItf $notFoundControllerProxy;

	public function __construct(
		RouteGeneratorsGeneratorItf $routeGeneratorsGenerator,
		ControllerProxyItf $notFoundControllerProxy
	)
	{
		$this->routeGeneratorsGenerator = $routeGeneratorsGenerator;
		$this->notFoundControllerProxy = $notFoundControllerProxy;
	}

	public function route(RequestItf $request):ControllerItf
	{
		/**
		 * @var RouteGeneratorItf $routeGenerator
		 */
		foreach($this->routeGeneratorsGenerator->generate() as $routeGenerator)
		{
			/**
			 * @var RouteItf $route
			 */
			foreach($routeGenerator->generate() as $route)
			{
				if ($route->getMatcher()->match($request))
				{
					return $route->getController();
				}
			}
		}

		return $this->notFoundControllerProxy->getController();
	}

}