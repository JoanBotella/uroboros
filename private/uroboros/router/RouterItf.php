<?php
declare(strict_types=1);

namespace uroboros\router;

use uroboros\request\RequestItf;
use uroboros\zone\page\controller\ControllerItf;

interface RouterItf
{

	public function route(RequestItf $request):ControllerItf;

}