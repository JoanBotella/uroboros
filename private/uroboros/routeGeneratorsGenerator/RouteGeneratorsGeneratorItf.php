<?php
declare(strict_types=1);

namespace uroboros\routeGeneratorsGenerator;

use Generator;
use uroboros\zone\page\route\RouteItf;

interface RouteGeneratorsGeneratorItf
{

	/**
	 * @return Generator<Generator<RouteItf>>
	 */
	public function generate():Generator;

}