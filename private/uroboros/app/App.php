<?php
declare(strict_types=1);

namespace uroboros\app;

use uroboros\app\AppItf;
use uroboros\localization\translationSetupper\TranslationSetupperItf;
use uroboros\requestBuilder\RequestBuilderItf;
use uroboros\router\RouterItf;
use uroboros\responseDispatcher\ResponseDispatcherItf;

final class App implements AppItf
{

	private RequestBuilderItf $requestBuilder;
	private TranslationSetupperItf $translationSetupper;
	private RouterItf $router;
	private ResponseDispatcherItf $responseDispatcher;

	public function __construct(
		RequestBuilderItf $requestBuilder,
		TranslationSetupperItf $translationSetupper,
		RouterItf $router,
		ResponseDispatcherItf $responseDispatcher
	)
	{
		$this->requestBuilder = $requestBuilder;
		$this->translationSetupper = $translationSetupper;
		$this->router = $router;
		$this->responseDispatcher = $responseDispatcher;
	}

	public function run():void
	{
		$request = $this->requestBuilder->build();

		$this->translationSetupper->setup($request);

		$controller = $this->router->route($request);

		$response = $controller->action($request);

		$this->responseDispatcher->dispatch($response);
	}

}