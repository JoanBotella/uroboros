<?php
declare(strict_types=1);

namespace uroboros\app;

interface AppItf
{

	public function run():void;

}