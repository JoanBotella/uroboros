<?php
declare(strict_types=1);

namespace uroboros\configuration;

use uroboros\configuration\ConfigurationItf;

final class Configuration implements ConfigurationItf
{

	private string $baseUrl;
	private string $defaultLanguageCode;
	/**
	 * @var array<string> $enabledLanguageCodes
	 */
	private array $enabledLanguageCodes;

	/**
	 * @param array<string> $enabledLanguageCodes
	 */
	public function __construct(
		string $baseUrl,
		string $defaultLanguageCode,
		array $enabledLanguageCodes
	)
	{
		$this->baseUrl = $baseUrl;
		$this->defaultLanguageCode = $defaultLanguageCode;
		$this->enabledLanguageCodes = $enabledLanguageCodes;
	}

	public function getBaseUrl():string
	{
		return $this->baseUrl;
	}

	public function getDefaultLanguageCode():string
	{
		return $this->defaultLanguageCode;
	}

	/**
	 * @return array<string>;
	 */
	public function getEnabledLanguageCodes():array
	{
		return $this->enabledLanguageCodes;
	}

}
