<?php
declare(strict_types=1);

namespace uroboros\configuration;

interface ConfigurationItf
{

	public function getBaseUrl():string;

	public function getDefaultLanguageCode():string;

	/**
	 * @return array<string>;
	 */
	public function getEnabledLanguageCodes():array;

}