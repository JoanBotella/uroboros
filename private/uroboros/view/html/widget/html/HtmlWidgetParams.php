<?php
declare(strict_types=1);

namespace uroboros\view\html\widget\html;

use uroboros\view\html\widget\html\HtmlWidgetFaviconItf;
use uroboros\view\html\widget\html\HtmlWidgetStylesheetItf;
use uroboros\localization\alternateTranslation\AlternateTranslationItf;

final class HtmlWidgetParams implements HtmlWidgetParamsItf
{

	/**
	 * @var array<string> $htmlClasses
	 */
	private array $htmlClasses;
	private string $languageCode;
	private string $canonicalHref;
	/**
	 * @var array<AlternateTranslationItf> $alternates
	 */
	private array $alternateTranslations;
	private string $titleContent;
	private HtmlWidgetFaviconItf $favicon;
	/**
	 * @var array<HtmlWidgetStylesheetItf> $stylesheets
	 */
	private array $stylesheets;
	private string $body;

	/**
	 * @param array<string> $htmlClasses
	 * @param array<AlternateTranslationItf> $alternates
	 * @param array<HtmlWidgetStylesheetItf> $stylesheets
	 */
	public function __construct(
		array $htmlClasses,
		string $languageCode,
		string $canonicalHref,
		array $alternateTranslations,
		string $titleContent,
		HtmlWidgetFaviconItf $favicon,
		array $stylesheets,
		string $body
	)
	{
		$this->htmlClasses = $htmlClasses;
		$this->languageCode = $languageCode;
		$this->canonicalHref = $canonicalHref;
		$this->alternateTranslations = $alternateTranslations;
		$this->titleContent = $titleContent;
		$this->favicon = $favicon;
		$this->stylesheets = $stylesheets;
		$this->body = $body;
	}

	/**
	 * @return array<string>
	 */
	public function getHtmlClasses():array
	{
		return $this->htmlClasses;
	}

	public function getLanguageCode():string
	{
		return $this->languageCode;
	}

	public function getCanonicalHref():string
	{
		return $this->canonicalHref;
	}

	/**
	 * @return array<AlternateTranslationItf>
	 */
	public function getAlternateTranslations():array
	{
		return $this->alternateTranslations;
	}

	public function getTitleContent():string
	{
		return $this->titleContent;
	}

	public function getFavicon():HtmlWidgetFaviconItf
	{
		return $this->favicon;
	}

	/**
	 * @return array<HtmlWidgetStylesheetItf>
	 */
	public function getStylesheets():array
	{
		return $this->stylesheets;
	}

	public function getBody():string
	{
		return $this->body;
	}

}