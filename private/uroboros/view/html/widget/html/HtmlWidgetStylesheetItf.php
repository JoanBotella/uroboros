<?php
declare(strict_types=1);

namespace uroboros\view\html\widget\html;

interface HtmlWidgetStylesheetItf
{

	public function getHref():string;

	public function getMedia():string;

}