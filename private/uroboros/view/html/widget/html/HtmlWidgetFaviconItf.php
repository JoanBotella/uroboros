<?php
declare(strict_types=1);

namespace uroboros\view\html\widget\html;

interface HtmlWidgetFaviconItf
{

	public function getHref():string;

	public function getType():string;

}