<?php
declare(strict_types=1);

namespace uroboros\view\html\widget\html;

use uroboros\configurationContainer\ConfigurationContainerItf;
use uroboros\view\html\widget\html\HtmlWidgetItf;
use uroboros\view\html\widget\html\HtmlWidgetParamsItf;
use uroboros\view\html\widget\html\HtmlWidgetContext;
use uroboros\view\html\widget\html\HtmlWidgetContextItf;

final class HtmlWidget implements HtmlWidgetItf
{

	private ConfigurationContainerItf $configurationContainer;

	public function __construct(
		ConfigurationContainerItf $configurationContainer
	)
	{
		$this->configurationContainer = $configurationContainer;
	}

	public function render(HtmlWidgetParamsItf $params):string
	{
		$context = $this->buildContext($params);
		return $this->renderByContext($context);
	}

		private function buildContext(HtmlWidgetParamsItf $params):HtmlWidgetContextItf
		{
			return new HtmlWidgetContext(
				$params->getHtmlClasses(),
				$params->getLanguageCode(),
				$this->getBaseHref(),
				$params->getCanonicalHref(),
				$params->getAlternateTranslations(),
				$params->getTitleContent(),
				$params->getFavicon(),
				$params->getStylesheets(),
				$params->getBody()
			);
		}

			private function getBaseHref():string
			{
				return $this->configurationContainer->get()->getBaseUrl();
			}

		private function renderByContext(HtmlWidgetContextItf $context):string
		{
			ob_start();
			require('htmlWidgetTemplate.php');
			return ob_get_clean();
		}

}