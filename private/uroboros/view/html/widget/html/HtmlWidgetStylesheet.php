<?php
declare(strict_types=1);

namespace uroboros\view\html\widget\html;

use uroboros\view\html\widget\html\HtmlWidgetStylesheetItf;

final class HtmlWidgetStylesheet implements HtmlWidgetStylesheetItf
{

	private string $href;
	private string $media;

	public function __construct(
		string $href,
		string $media
	)
	{
		$this->href = $href;
		$this->media = $media;
	}

	public function getHref():string
	{
		return $this->href;
	}

	public function getMedia():string
	{
		return $this->media;
	}

}