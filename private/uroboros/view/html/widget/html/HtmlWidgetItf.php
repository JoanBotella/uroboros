<?php
declare(strict_types=1);

namespace uroboros\view\html\widget\html;

use uroboros\view\html\widget\html\HtmlWidgetParamsItf;

interface HtmlWidgetItf
{

	public function render(HtmlWidgetParamsItf $params):string;

}