<?php
declare(strict_types=1);

namespace uroboros\view\html\widget\html;

use uroboros\view\html\widget\html\HtmlWidgetFaviconItf;
use uroboros\view\html\widget\html\HtmlWidgetStylesheetItf;
use uroboros\localization\alternateTranslation\AlternateTranslationItf;

interface HtmlWidgetContextItf
{

	/**
	 * @return array<string>
	 */
	public function getHtmlClasses():array;

	public function getLanguageCode():string;

	public function getBaseHref():string;

	public function getCanonicalHref():string;

	/**
	 * @return array<AlternateTranslationItf>
	 */
	public function getAlternateTranslations():array;

	public function getTitleContent():string;

	public function getFavicon():HtmlWidgetFaviconItf;

	/**
	 * @return array<HtmlWidgetStylesheetItf>
	 */
	public function getStylesheets():array;

	public function getBody():string;

}