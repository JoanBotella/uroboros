<?php
declare(strict_types=1);

namespace uroboros\view\html\widget\html;

use uroboros\view\html\widget\html\HtmlWidgetFaviconItf;

final class HtmlWidgetFavicon implements HtmlWidgetFaviconItf
{

	private string $href;
	private string $type;

	public function __construct(
		string $href,
		string $type
	)
	{
		$this->href = $href;
		$this->type = $type;
	}

	public function getHref():string
	{
		return $this->href;
	}

	public function getType():string
	{
		return $this->type;
	}

}