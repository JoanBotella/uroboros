<?php

use uroboros\view\html\widget\html\HtmlWidgetContextItf;

/**
 * @var HtmlWidgetContextItf $context
 */

$htmlClassAttribute = ' class="';
$htmlClasses = $context->getHtmlClasses();
if (!empty($htmlClasses))
{
	$htmlClassAttribute .= implode(' ', $htmlClasses);
}
$htmlClassAttribute .= '"';

?><!DOCTYPE html>
<html lang="<?=$context->getLanguageCode()?>"<?=$htmlClassAttribute?>>
<head>
	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	<link rel="canonical" href="<?=$context->getCanonicalHref()?>" />

<?php foreach($context->getAlternateTranslations() as $alternate) { ?>
	<link rel="alternate" hreflang="<?=$alternate->getLanguageCode()?>" href="<?=$alternate->getUrl()?>" />
<?php } ?>

	<base href="<?=$context->getBaseHref()?>" />

	<link rel="icon" href="<?=$context->getFavicon()->getHref()?>" type="<?=$context->getFavicon()->getType()?>" />

	<title><?=$context->getTitleContent()?></title>

<?php foreach($context->getStylesheets() as $stylesheet) { ?>
	<link rel="stylesheet" href="<?=$stylesheet->getHref()?>" media="<?=$stylesheet->getMedia()?>" />
<?php } ?>
</head>
<?=$context->getBody()?>
</html>