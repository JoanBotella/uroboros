<?php
declare(strict_types=1);

namespace uroboros\view\html;

use uroboros\http\HttpConstant;
use uroboros\response\Response;
use uroboros\response\ResponseItf;
use uroboros\view\html\widget\html\HtmlWidgetFavicon;
use uroboros\view\html\widget\html\HtmlWidgetFaviconItf;
use uroboros\view\html\widget\html\HtmlWidgetItf;
use uroboros\view\html\widget\html\HtmlWidgetParams;
use uroboros\view\html\widget\html\HtmlWidgetParamsItf;
use uroboros\view\html\widget\html\HtmlWidgetStylesheetItf;
use uroboros\localization\alternateTranslation\AlternateTranslationItf;
use uroboros\mimeType\MimeTypeConstant;

abstract class HtmlViewAbs
{

	private HtmlWidgetItf $htmlWidget;

	/**
	 * @var array<AlternateTranslationItf> $alternateTranslations
	 */
	private array $alternateTranslations;

	public function __construct(
		HtmlWidgetItf $htmlWidget
	)
	{
		$this->htmlWidget = $htmlWidget;
	}

	protected function buildResponse():ResponseItf
	{
		return new Response(
			$this->getHttpStatusCode(),
			$this->renderHtmlWidget()
		);
	}

		protected function getHttpStatusCode():int
		{
			return HttpConstant::STATUS_CODE_OK;
		}

		protected function renderHtmlWidget():string
		{
			return $this->htmlWidget->render(
				$this->buildHtmlWidgetParams()
			);
		}

			protected function buildHtmlWidgetParams():HtmlWidgetParamsItf
			{
				return new HtmlWidgetParams(
					$this->getHtmlClasses(),
					$this->getLanguageCode(),
					$this->getCanonicalHref(),
					$this->getAlternateTranslations(),
					$this->getTitleContent(),
					$this->getFavicon(),
					$this->getStylesheets(),
					$this->renderBodyWidget()
				);
			}

				/**
				 * @return array<string>
				 */
				protected function getHtmlClasses():array
				{
					return [];
				}

				abstract protected function getLanguageCode():string;

				abstract protected function getCanonicalHref():string;

				/**
				 * @return array<AlternateTranslationItf>
				 */
				protected function getAlternateTranslations():array
				{
					if (!isset($this->alternateTranslations))
					{
						$this->alternateTranslations = $this->buildAlternateTranslations();
					}
					return $this->alternateTranslations;
				}

					/**
					 * @return array<AlternateTranslationItf>
					 */
					protected function buildAlternateTranslations():array
					{
						return [];
					}

				protected function getTitleContent():string
				{
					return $this->getPageTitle().' | '.$this->getSiteTitle();
				}

					abstract protected function getPageTitle():string;

					abstract protected function getSiteTitle():string;
			
				abstract protected function renderBodyWidget():string;

				protected function getFavicon():HtmlWidgetFaviconItf
				{
					return new HtmlWidgetFavicon(
						'img/favicon.png',
						MimeTypeConstant::PNG
					);
				}

				/**
				 * @return array<HtmlWidgetStylesheetItf>
				 */
				protected function getStylesheets():array
				{
					return [];
				}

}