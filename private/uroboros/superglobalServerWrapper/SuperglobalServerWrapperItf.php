<?php
declare(strict_types=1);

namespace uroboros\superglobalServerWrapper;

interface SuperglobalServerWrapperItf
{

	public function getRequestUri():string;

	public function getHttpAcceptLanguage():string;

}