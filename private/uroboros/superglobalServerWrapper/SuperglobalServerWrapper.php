<?php
declare(strict_types=1);

namespace uroboros\superglobalServerWrapper;

use uroboros\superglobalServerWrapper\SuperglobalServerWrapperItf;

final class SuperglobalServerWrapper implements SuperglobalServerWrapperItf
{

	public function getRequestUri():string
	{
		return $_SERVER['REQUEST_URI'];
	}

	public function getHttpAcceptLanguage():string
	{
		return $_SERVER['HTTP_ACCEPT_LANGUAGE'];
	}

}