<?php
declare(strict_types=1);

require __DIR__.'/../composer/vendor/autoload.php';

use blog\dependencyInjector\BlogDependencyInjector;

function main()
{
	$dependencyInjector = new BlogDependencyInjector();
	$app = $dependencyInjector->getApp();
	$app->run();
}