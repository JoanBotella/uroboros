# joanbotella.com

Personal blog, developed alongside the Uroboros custom web framework.

## Project status

Early development stage, but ready for production.

## License

GNU GPL v3 or later <https://www.gnu.org/licenses/gpl-3.0.en.html>

## Author

Joan Botella Vinaches <https://joanbotella.com>