
let disableEmaiAnchor = function ()
{
	let emailAnchorElement = document.querySelector('[data-hook="blog-front-contact-main_body__email_anchor"]');

	if (emailAnchorElement == null)
	{
		return;
	}

	let preventDefault = function (event)
	{
		event.preventDefault();
		return false;
	};

	emailAnchorElement.addEventListener('click', preventDefault);
	emailAnchorElement.addEventListener('auxclick', preventDefault);
};

document.addEventListener('DOMContentLoaded', disableEmaiAnchor);
